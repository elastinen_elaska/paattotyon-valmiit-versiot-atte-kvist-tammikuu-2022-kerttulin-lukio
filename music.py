import pygame, json, random, os


class Music_player_state:
    def __init__(self):
        self.song_list = list(os.listdir("music/theme_songs/"))
        self.song_id = random.randint(0, len(self.song_list)-1)
        self.song = self.song_list[self.song_id]

    def cycle_song(self):
        self.song_id += 1
        if self.song_id >= len(self.song_list):
            self.song_id = 0
        self.song = self.song_list[self.song_id]
        return "music/theme_songs/" + self.song


music_player_state = Music_player_state()

def load_volume():
    with open("settings/settings.json", "r") as jsonfile:
        data = json.load(jsonfile)
    volume = data["volume"]

    return float(volume)

def get_playing_song_name_string():
    song = music_player_state.song.capitalize()
    song = song.replace("_", " ")
    song = song[:len(song)-3]
    return song

def get_playing_song():
    return "music/theme_songs/" + music_player_state.song


def set_song(player):
    player.stop()
    player.load(music_player_state.cycle_song())
    player.play(-1)


def initialize_playing_music(music_player_state):

    song = get_playing_song()

    volume = load_volume()

    music_player_state.set_volume(volume)
    music_player_state.load(song)
    music_player_state.play(-1)



