import pygame, sys, json, music

sys.path.insert(0, "classes")


import ui_elements, fonts, button_classes, helpers



switch_button_rect = pygame.Rect(950, 100, 50, 40)


def save_settings(new_volume):
    with open("settings/settings.json", "r") as jsonfile:
        data = json.load(jsonfile)

    data["volume"] = new_volume

    with open("settings/settings.json", "w") as jsonfile:
        json.dump(data, jsonfile)

def initialize_settings_screen(surface, exit_button, volume_slider, volume):
    surface.fill("lightblue")
    exit_button.draw(surface)

    volume_slider.draw(surface)
    surface.blit(helpers.draw_text(f"Äänenvoimakkuus: {round(volume*100)}", fonts.title_screen_font, "black"), (1200, 100))
    surface.blit(helpers.draw_text(f"%", fonts.title_screen_font, "black"), (1550, 102))
    surface.blit(helpers.draw_text(f"Nyt soimassa:  {music.get_playing_song_name_string()}", fonts.title_screen_font, "black"), (1200, 180))

    pygame.draw.rect(surface, "black", switch_button_rect)
    pygame.draw.polygon(surface, "white", ((960, 110), (990, 120), (960, 130)))

    surface.blit(helpers.draw_text(f"Vaihda fps painamalla funktionappeja pelin aikana: f6=60, f5=50, f4=40, f3=30", fonts.title_screen_font, "black"), (300, 800))

    pygame.display.flip()

def begin(surface, clock, next_scene):
    exit_image = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()
    exit_button = button_classes.Button(50, 50, exit_image, 1)


    volume = music.load_volume()

    volume_slider = ui_elements.Slider_horizontal("blue", 600, 100, 300, 40, "grey", "red", volume)

    initialize_settings_screen(surface, exit_button, volume_slider, volume)


    in_settings = True
    while in_settings:

        pos = pygame.mouse.get_pos()
        left_click_pressed = pygame.mouse.get_pressed()[0]

        if left_click_pressed and volume_slider.collides_with_ball(pos[0], pos[1]):
            volume_slider.currently_pressed = True
            while True:
                pygame.event.pump()
                if not pygame.mouse.get_pressed()[0]:
                    break

                pygame.draw.rect(surface, "lightblue", (1150, 90, 400, 60))

                volume = volume_slider.draw(surface)
                pygame.mixer.music.set_volume(volume)

                initialize_settings_screen(surface, exit_button, volume_slider, volume)


            volume_slider.currently_pressed = False
            volume_slider.draw(surface)
            initialize_settings_screen(surface, exit_button, volume_slider, volume)

        if exit_button.check_logic():
            helpers.button_clicked(exit_button, surface)
            in_settings = False

        

        if left_click_pressed and switch_button_rect.collidepoint(pos[0], pos[1]):
            music.set_song(pygame.mixer.music)
            initialize_settings_screen(surface, exit_button, volume_slider, volume)
            helpers.avoid_second_click()



        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        


        pygame.display.flip()
        clock.tick(60)


    helpers.avoid_second_click()
    save_settings(volume)
    pygame.mixer.music.set_volume(music.load_volume())
    
    volume_slider = None # avoid memory leak
    exit_image = None
    exit_button = None


    return next_scene

