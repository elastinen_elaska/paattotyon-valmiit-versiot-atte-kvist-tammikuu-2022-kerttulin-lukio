import pygame, helpers, sys

sys.path.insert(0, "classes")

import button_classes


# image loading

spring_over_world = ["spring_water_tile.png", "spring_grass_tile.png", "spring_water_weed_tile.png",
                 "spring_corner_tile_left_up.png", "spring_corner_tile_right_up.png", "spring_corner_tile_left_down.png",
                 "spring_corner_tile_right_down.png", "spring_rock_tile.png", "asphalt_road.png", "sand_tile.png",
                 "spring_full_weed_tile.png", "spring_lumpeet.png", "spring_big_rock_tile.png"]

autumn_over_world = ["autumn_water_tile.png", "autumn_grass_tile.png", "autumn_water_weed_tile.png",
                 "autumn_corner_tile_left_up.png", "autumn_corner_tile_right_up.png", "autumn_corner_tile_left_down.png",
                 "autumn_corner_tile_right_down.png", "autumn_rock_tile.png", "asphalt_road.png", "sand_tile.png",
                 "autumn_full_weed_tile.png", "autumn_lumpeet.png", "autumn_big_rock_tile.png"]


picture_list_underwater = ["empty_depth_tile.png", "depth=1_tile.png",
        "depth=2_tile.png", "depth=3_tile.png", "depth=4_tile.png", "depth=5_tile.png",
        "depth=6_tile.png", "depth=7_tile.png", "depth=8_tile.png", "depth=9_tile.png",
        "depth=10_tile.png"]

image_list_over_world= []
button_list_over_world = []
button_select_list_over_world = []

image_list_underwater = []
button_list_underwater = []
button_select_list_underwater = []


def load_and_create_image(TILE_SIZE, pictures_list):
    array = []
    for loading_image in pictures_list:
        tile = pygame.image.load(f"graphics/tileimages/{loading_image}").convert()
        tile = pygame.transform.scale(tile, (TILE_SIZE, TILE_SIZE))
        array.append(tile)
    return tuple(array)

# buttons

def add_to_button_list(image_list):
    temp = []
    y_cor = 330
    x_cor = 500
    for x in range(len(image_list)):
        if x % 9 == 0:
            y_cor += 100
            x_cor = 500

        button = image_list[x]
        button = pygame.transform.scale(button, (55, 55))
        temp.append(button_classes.Button(x_cor, y_cor, button, 1))
        x_cor += 75
    return temp


def draw_world_data_in_editor(world_data, scroll_x, scroll_y, surface, TILE_SIZE, off_set_from_panel, image_list, rows, columns):
    for current_looping_row, row in enumerate(world_data[scroll_y:scroll_y+rows]):
        for current_looping_column, tile in enumerate(row[scroll_x:scroll_x+columns]):
            if tile > -1:
                surface.blit(image_list[tile], (off_set_from_panel+current_looping_column*TILE_SIZE, current_looping_row*TILE_SIZE))

def init_map_surf(lake):
    map_surf = pygame.surface.Surface((lake.world_size_x, lake.world_size_y))
    for current_looping_row, row in enumerate(lake.over_world):
        for current_looping_column, tile in enumerate(row):
            if tile > -1:
                map_surf.blit(image_list_over_world[tile], (current_looping_column*lake.TILE_SIZE, current_looping_row*lake.TILE_SIZE))
    return map_surf

















