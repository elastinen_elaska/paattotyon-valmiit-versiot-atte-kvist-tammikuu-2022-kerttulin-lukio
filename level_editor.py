import json, csv, data_handling, world_drawing, fonts
import sys, time, pygame, helpers, pickle, random

sys.path.insert(0, "classes")

import button_classes
import fish_info_classes

import matplotlib
import parameter_box




# easy to use instances from this class for multiple edit datas
class Drawing_data:
    def __init__ (self, data_to_use, lake_name_to_save, tiles_to_draw, pictures_list, buttons_list):
        self.data_to_use = data_to_use
        self.lake_name_to_save = lake_name_to_save
        self.tiles_to_draw = tiles_to_draw
        self.pictures_list = pictures_list
        self.buttons_list = buttons_list





scroll_x = 0
scroll_y = 0



#lake_name = input("What is the name of the lake you want to edit? ")
#lake_name = "piikkiönlahti"
lake_name = "sattajärvi"
lake_name = "kivijärvi"
lake_name = "kolkka"

season = "spring"
season = "autumn"

try:
    over_world_init_data = data_handling.load_existing_level_data_list(lake_name, "draw_data.pickle")

    underwater_init_data = data_handling.load_existing_level_data_list(lake_name, "underwater_data.pickle")

    with open(f"lakes/{lake_name}/data.json") as file:
        level_parameters = json.load(file)

    print("Files opened successfully.")

except FileNotFoundError:
    print("File not found. Lake is removed or it isn't created yet.")
    answer = input("File not found. Create new one? (y/n)")
    if answer ==  "y":
        data_handling.create_new_level_json_file(lake_name)
        print("json file created.")
        data_handling.create_new_fish_pickle_file(lake_name)
        over_world_init_data = data_handling.load_level_data(lake_name, "draw_data.pickle")
        print("empty over world data created.")
        underwater_init_data = data_handling.load_level_data(lake_name, "underwater_data.pickle")
    else:
        print("Terminating program. File not created.")
        sys.exit()


pygame.init()

screen_width = 1920
screen_height = 1080
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()


pygame.display.set_caption("Järvieditori")


# game variables

panel_size_x = 500


# constant buttons and images

exit_surface = pygame.image.load("graphics/buttonimages/exit_button.png").convert()
exit_button = button_classes.Button_with_function(100, 100, exit_surface, 1, None)

tile_button_text_surface = pygame.image.load("graphics/buttonimages/tiles_button.png").convert()
tiles_button = button_classes.Button_with_function(100, 100, tile_button_text_surface, 0.5, "draw_buttons")

fish_button_surface = pygame.image.load("graphics/buttonimages/fish_button.png").convert()
fish_button = button_classes.Button_with_function(100, 265, fish_button_surface, 0.5, "place_fish")

save_fish_surface = pygame.image.load("graphics/buttonimages/save_fish_data_button.png").convert()
save_fish_data_button = button_classes.Button_with_function(100, 440, save_fish_surface, 1, "pickle_fish")

load_fish_surface = pygame.image.load("graphics/buttonimages/load_fish_data_button.png").convert()
load_fish_data_button = button_classes.Button_with_function(210, 440, load_fish_surface, 1, "load_pickled_fish")

delete_fish_surface = pygame.image.load("graphics/buttonimages/delete_fish_button.png").convert()
delete_fish_button = button_classes.Button_with_function(100, 550, delete_fish_surface, 1, "delete_fish")

use_buttons_list = [tiles_button, fish_button, save_fish_data_button, load_fish_data_button, delete_fish_button]

exit_panel_surf = pygame.image.load("graphics/buttonimages/close_panel.png").convert()
close_panel_button = button_classes.Button(1200, 400, exit_panel_surf, 1)




# defined text which doesn't change during execution
save_button_text_surface = fonts.basic_font.render("Save", False, "black")
switch_editing_data = fonts.small_font.render("Switch data", False, "black")
click_fish_position = fonts.small_font.render("Click coordinates for fish", False, "black")



world_size_x = level_parameters["world_size_x"]
world_size_y = level_parameters["world_size_y"]
const_TILE_SIZE = TILE_SIZE = level_parameters["tile_size"]

world_drawing.image_list_over_world = world_drawing.load_and_create_image(TILE_SIZE, world_drawing.spring_over_world)
world_drawing.image_list_underwater = world_drawing.load_and_create_image(TILE_SIZE, world_drawing.picture_list_underwater)
world_drawing.button_list_over_world = world_drawing.add_to_button_list(world_drawing.image_list_over_world)
world_drawing.button_list_underwater = world_drawing.add_to_button_list(world_drawing.image_list_underwater)

COLUMNS = 1920 // TILE_SIZE
ROWS = 1080 // TILE_SIZE

over_world_data = Drawing_data(over_world_init_data, "draw_data.pickle", world_drawing.image_list_over_world, world_drawing.spring_over_world, world_drawing.button_list_over_world)

underwater_data = Drawing_data(underwater_init_data, "underwater_data.pickle", world_drawing.image_list_underwater, world_drawing.picture_list_underwater, world_drawing.button_list_underwater)





print("SEASON IS CURRENTLY SPRING. REMEMBER TO CHANGE IT")

fish_objects_list = data_handling.load_pickled_fish(lake_name, season)


edit_datas_list = (over_world_data, underwater_data)

current_edit_data = 0


# calculate required lines to draw the grid

if world_size_y > screen_height:
    amount_of_horizontal_lines = screen_height // TILE_SIZE + 1
    vertical_line_end_point = screen_height
else:
    amount_of_horizontal_lines = world_size_y // TILE_SIZE + 1
    vertical_line_end_point = world_size_y

if world_size_x > screen_width-panel_size_x:
    amount_of_vertical_lines = (screen_width-panel_size_x) // TILE_SIZE +1
    horizontal_line_end_point = screen_width
else:
    amount_of_vertical_lines = world_size_x // TILE_SIZE +1 
    horizontal_line_end_point = world_size_x+panel_size_x



def flood_fill(start_x, start_y, start_color, color_to_update, matrix):
    width = len(matrix)
    height = len(matrix[0])

    def fill(x, y, start_color, color_to_update):
        if matrix[x][y] != start_color:
            return
        elif matrix[x][y] == color_to_update:
            return
        else:
            matrix[x][y] = color_to_update
            neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
            for n in neighbors:
                if 0 <= n[0] <= width-1 and 0 <= n[1] <= height-1:
                    fill(n[0], n[1], start_color, color_to_update)

    fill(start_x, start_y, start_color, color_to_update)
    return matrix



def delete_fish(fish_objects_list):
    for num in fish_objects_list:
        print(num.coordinates)

    i = input(("which index do you want to remove (INDEX!! SO REMEMBER -1 !!!\nTo not remove type n"))
    if i == "n":
        return fish_objects_list

    i = int(i)
    del fish_objects_list[i]

    print("deleted fish data")
    return fish_objects_list



def place_fish():
    global fish_objects_list
    pygame.draw.rect(screen, (212, 201, 171), (0, 0, panel_size_x, 1080))
    time.sleep(0.1)
    pygame.display.flip()
    coords = []
    screen.blit(click_fish_position, (100, 100))
    drawing_coords = []

    running = True
    while running:
        mouse_pos = pygame.mouse.get_pos()

        world_x = ((mouse_pos[0]+TILE_SIZE*scroll_x-500)//TILE_SIZE) * const_TILE_SIZE + mouse_pos[0] % TILE_SIZE
        world_y = ((mouse_pos[1]+TILE_SIZE*scroll_y) // TILE_SIZE) * const_TILE_SIZE + mouse_pos[1] % TILE_SIZE

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN and mouse_pos[0] >= 500:
                coords.append((world_x, world_y))
                drawing_coords.append((mouse_pos[0], mouse_pos[1]))

        
        exit_button.draw(screen)
        if exit_button.check_logic():
            time.sleep(0.1)

            new_fish_school = parameter_box.add_fish_school(screen, coords)
            
            running = False

            time.sleep(0.2)


        if len(coords) >= 3:
            pygame.draw.polygon(screen, "black", drawing_coords)

        pygame.time.wait(5)
        pygame.display.flip()

    fish_objects_list.append(new_fish_school)

    time.sleep(0.1)


def draw_fish_positions():
    world_drawing.draw_world_data_in_editor(edit_datas_list[current_edit_data].data_to_use, scroll_x, scroll_y, screen, TILE_SIZE, panel_size_x, edit_datas_list[current_edit_data].tiles_to_draw, ROWS, COLUMNS)
    seeing_fish = True
    while seeing_fish:

        for fish in fish_objects_list:
            coords = [list(x) for x in fish.coordinates]
            for x in coords:
                original_x_tile = x[0] // const_TILE_SIZE
                x[0] = TILE_SIZE*original_x_tile
                x[0] -= scroll_x*TILE_SIZE
                x[0] += 500

                original_y_tile = x[1] // const_TILE_SIZE
                x[1] = TILE_SIZE*original_y_tile
                x[1] -= scroll_y*TILE_SIZE

            pygame.draw.polygon(screen, "black", coords)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if pygame.key.name(event.key) == "f7":
                    print("returned")
                    seeing_fish = False


        pygame.time.wait(50)
        pygame.display.flip()

def draw_panel(button_color: str, is_saved: str):
    pygame.draw.rect(screen, (0, 168, 184), (0, 0, panel_size_x, 1080))
    
    pygame.draw.rect(screen, "white", (100, 800, 200, 100))
    screen.blit(save_button_text_surface, (110, 810))

    saved_text = fonts.small_font.render(is_saved, False, "black")
    screen.blit(saved_text, (110, 920))



def initialize_tile_buttons(button_list):
    pygame.draw.rect(screen, "grey", (400, 400, 900, 400))

    close_panel_button.draw(screen)

    for button in button_list:
        button.draw(screen)
        if button.clicked:
            helpers.button_clicked(button, screen)


    pygame.display.flip()


def draw_buttons(button_list):
    global is_level_saved_text

    # water is the chosen default tile
    current_tile = 0

    initialize_tile_buttons(button_list)
    
    running = True
    while running:
        if close_panel_button.check_logic():
            helpers.button_clicked(close_panel_button, screen)
            running = False

        button_count = 0
        for button_count, i in enumerate(button_list):
            if i.check_logic():
                initialize_tile_buttons(button_list)    
                helpers.button_clicked(i, screen)
                current_tile = button_count
                is_level_saved_text = "Not saved"

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        pygame.display.flip()
        clock.tick(60)
        pygame.time.wait(5)
    draw_world()
    return current_tile

def check_buttons():
    global current_tile, fish_objects_list
    for button in use_buttons_list:
        button.draw(screen)
        if button.check_logic():
            helpers.button_clicked(button, screen)
            func = button.return_function()
            if func == "draw_buttons":
                current_tile = globals()[func](edit_datas_list[current_edit_data].buttons_list)
            if func == "place_fish":
                 globals()[func]()
            if func == "pickle_fish":
                data_handling.pickle_fish(lake_name, fish_objects_list, season)
            if func == "load_pickled_fish":
                fish_objects_list = data_handling.load_pickled_fish(lake_name, season)
            if func == "delete_fish":
                fish_objects_list = delete_fish(fish_objects_list)


def draw_world():
    screen.fill("black")
    world_drawing.draw_world_data_in_editor(edit_datas_list[current_edit_data].data_to_use, scroll_x, scroll_y, screen, TILE_SIZE, panel_size_x, edit_datas_list[current_edit_data].tiles_to_draw, ROWS, COLUMNS)
    draw_panel("red", is_level_saved_text)
    check_buttons()
    pygame.draw.rect(screen, switch_data_color, (90, 690, 200, 100))
    screen.blit(switch_editing_data, (100, 700))
    screen.blit(helpers.draw_text(season, fonts.title_screen_font, "black"), (100, 50))
    pygame.display.flip()


current_tile = -1
draw_on_exit = False
able_to_draw = True
is_level_saved_text = "not saved"
running = True
user_wants_fps = False
switch_data_color = "blue"

lake_name_to_save = f"{lake_name}/{lake_name}_draw_data.pickle"

draw_world()

while running:

    mouse_on_tile_button = False


    mouse_pos = pygame.mouse.get_pos()


    #grid position for tiles
    grid_column = (mouse_pos[0]+TILE_SIZE*scroll_x-500) // TILE_SIZE
    grid_row = (mouse_pos[1]+TILE_SIZE*scroll_y) // TILE_SIZE

    able_to_draw = mouse_pos[0] >= 500

    if mouse_pos[0] > 100 and mouse_pos[1] > 800 and mouse_pos[0] < 400 and mouse_pos[1] < 900 and pygame.mouse.get_pressed()[0] == 1:
        data_handling.save_level_data(lake_name, edit_datas_list[current_edit_data].lake_name_to_save, edit_datas_list[current_edit_data].data_to_use)
        able_to_draw = False
        is_level_saved_text = "saved"
        pygame.display.flip()
        

    if mouse_pos[0] > 100 and mouse_pos[1] > 100 and mouse_pos[0] < 300 and mouse_pos[1] < 200 and able_to_draw:
        draw_panel("blue", is_level_saved_text)
        mouse_on_tile_button = True
    else:
        draw_panel("red", is_level_saved_text)

    if pygame.mouse.get_pressed()[0] == 1 and able_to_draw and grid_column >= 0:
        draw_world()
        try:
            edit_datas_list[current_edit_data].data_to_use[grid_row][grid_column] = current_tile
            is_level_saved_text = "not saved"
        except IndexError:
            print("TILE CLICKED OUTSIDE OF CSV FILE'S RANGE!!!")
        draw_world()

    if pygame.mouse.get_pressed()[2] == 1 and able_to_draw and grid_column >= 0:
        try:
            edit_datas_list[current_edit_data].data_to_use[grid_row][grid_column] = -1
            is_level_saved_text = "not saved"
        except IndexError:
            print("TRIED TO REMOVE TILE OUTSIDE OF CSV FILE'S RANGE!!!")
        draw_world()

    if pygame.mouse.get_pressed()[1] == 1 and grid_column >= 0:
        try:
            edit_datas_list[current_edit_data].data_to_use = flood_fill(grid_row, grid_column, edit_datas_list[current_edit_data].data_to_use[grid_row][grid_column], current_tile, edit_datas_list[current_edit_data].data_to_use)
            is_level_saved_text = "not saved"

        except RecursionError:
            pass
        draw_world()


    if mouse_pos[0] > 90 and mouse_pos[0] < 290 and mouse_pos[1] > 700 and mouse_pos[1] < 800:
        switch_data_color = "red"
    else:
        switch_data_color = "blue"

    if switch_data_color == "red" and pygame.mouse.get_pressed()[0] == 1:
        current_edit_data += 1
        time.sleep(0.1)
        if current_edit_data == len(edit_datas_list):
            current_edit_data = 0

        draw_world()
        time.sleep(0.1)


    check_buttons()
    
    pressed_keys = pygame.key.get_pressed()

    # scroll the map
    if pressed_keys[pygame.K_LEFT] and scroll_x > 0:
        scroll_x -= 1
        draw_world()
    if pressed_keys[pygame.K_RIGHT]:
        scroll_x += 1
        draw_world()

    if pressed_keys[pygame.K_UP] and scroll_y > 0:
        scroll_y -= 1
        draw_world()

    if pressed_keys[pygame.K_DOWN]:
        scroll_y += 1
        draw_world()
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
        if event.type == pygame.KEYDOWN:
            if pygame.key.name(event.key) == "f8":
                user_wants_fps = not user_wants_fps
                pygame.draw.rect(screen, "black", (1599, 99, 300, 100))
            
            if pygame.key.name(event.key) == "f7":
                draw_fish_positions()

        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 4:
                TILE_SIZE += 1
            if event.button == 5:
                if TILE_SIZE > 1:
                    TILE_SIZE -= 1

            COLUMNS = 1920 // TILE_SIZE
            ROWS = 1080 // TILE_SIZE
            edit_datas_list[current_edit_data].tiles_to_draw = []
            edit_datas_list[current_edit_data].tiles_to_draw = world_drawing.load_and_create_image(TILE_SIZE, edit_datas_list[current_edit_data].pictures_list)
            draw_world()

    if user_wants_fps: 
        helpers.draw_grid(screen, edit_datas_list[current_edit_data].data_to_use, TILE_SIZE)
        screen.blit(helpers.draw_fps_text(screen, clock), (1600, 100))

    clock.tick(35)
    pygame.time.wait(5)
 
pygame.quit()


