Name

MATO-kalastussimulaattori / MATO fishing simulator

Description

This project is a fishing game based on real world lakes. Choose your lake, choose your bait and hope you catch something. Eventually you will improve your fishing knowledge and skills, which will lead you to catching even more fish. When you have honed your skills enough in practice mode, challenge computer players to a tournament and see if you've got what it takes to beat the best fishermen of this century. This project was created by Atte Kvist for Kerttuli upper secondary grade as a final project. 


Lakes in this game are built to be as realistic as possible, so real life fishing knowledge will come in handy. People who have fished in real life can see the realism in this game immediately, though fish are a little bigger than in real life and a little less rare, because nobody wants to play a game where 90 % of catches are small roach and perch.


This game is created using the PyGame library and Python3.8 programming language. The Matplotlib external library is also used. See requirements and licence.

This game is developed and tested in Linux Mint 20, but it should work at least on all linux environments with a display of at least 1920x1080 pixels.

Visuals:

Finnish YouTube-video created by me that explains the whole game in depth: https://www.youtube.com/watch?v=o_9ZwUiiEnI


Installation

1. cd existing/directory
2. git clone https://gitlab.com/koodauksia/paattotyoongintapeli.git
3. Make sure you have the required packages and all "Requirements" are met.
4. Run main_game.py 


Playing

1. Make sure you resolution of full hd or higher on a 16:9 aspect ratio display.
2. cd directory/where/game/is/installed
3. Use command pygame3 main_game.py
4. Start playing the game.



Requirements:

For the game to display correctly, you must use a 16:9 aspect ratio display and a resolution of at least full hd (or higher, but then the game is just not full screen.) If these requirements aren't met, the game will not work, or it will work incorrectly with a lot of drawing related bugs.

This project requires the matplotlib library for Python, which can be installed with
pip install matplotlib

PyGame library is also required:

pip install pygame

Many python standard library modules are required, but they should be installed already. 


Authors and acknowledgment

All programming: Atte Kvist
Game design: Atte Kvist
All Graphics: Atte Kvist

Special acknowledgment to Lauri Luostarinen, Kerttuli upper secondary grade, for all composing and creation of music used in this game.

Huge thanks to the authors of PyGame and Matplotlib libraries which make developing this project possible.


License

This project uses the external PyGame and Matplotlib libraries for Python3 and their licences are to be taken into consideration.
PyGame is licensed under the LGPL license. For more information see: https://www.gnu.org/licenses/lgpl-3.0.html
PyGame is unmodified for this project and is used "as is."

Matplolib library license can be found here: https://matplotlib.org/stable/users/project/license.html
Matplotlib library license states that "Non-BSD compatible licenses (e.g., LGPL, which PyGame is licensed under) are acceptable in matplotlib toolkits."

Under the licences of PyGame and Matplotlib library, this game can be used for any use, forever, by whoever wants. Everything is allowed that is also allowed under the Matplotlib and PyGame licenses. 
