
import pygame, fonts, json



def button_clicked(button, surface):
    button.draw_clicked_state(surface)
    pygame.display.flip()
    avoid_second_click()

def draw_fps_text(surface, clock):
    pygame.draw.rect(surface, "white", (1599, 99, 300, 100))
    fps_text = fonts.start_game_font.render(f"FPS: {clock.get_fps():.0f}", False, "black")
    return fps_text

def avoid_second_click():
    flag = True
    while flag:
        pygame.event.pump()
        flag = pygame.mouse.get_pressed()[0]

def text_clicked(text, surface):
    text.font_color = text.active_font_color
    text.draw(surface)
    pygame.display.flip()
    avoid_second_click()

def draw_text(text, font, color):
    return font.render(text, True, color)

def draw_grid(surface, world_data, TILE_SIZE):
    # vertical lines
    vertical_line_amount = 1420//TILE_SIZE
    horizontal_line_amount = 1080 // TILE_SIZE
    horizontal_end_point = 1920
    vertical_end_point = 1080

    if len(world_data[0]) * TILE_SIZE < 1420:
        vertical_line_amount = len(world_data[0])+1
        horizontal_end_point = TILE_SIZE*len(world_data[0])+500

    if len(world_data) * TILE_SIZE < 1080:
        horizontal_line_amount = len(world_data)+1
        vertical_end_point = TILE_SIZE*len(world_data)

    for x in range(vertical_line_amount):
        pygame.draw.line(surface, "white", (x*TILE_SIZE+500, 0), (x*TILE_SIZE+500, vertical_end_point))

    # horizontal lines    
    for x in range(horizontal_line_amount):
        pygame.draw.line(surface, "white", (500, x * TILE_SIZE), (horizontal_end_point, x*TILE_SIZE))








