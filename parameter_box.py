import pygame

import sys
sys.path.insert(0, "classes")
import ui_elements, fonts, helpers

#### "from" syntax is needed to import fish classes properly
from fish_info_classes import *
import button_classes


def add_fish_school(surface, coords):
    left = 500
    height = 68
    start = 150


    exit_surf = pygame.image.load("graphics/buttonimages/exit_button.png").convert()
    exit_button = button_classes.Button(50, 250, exit_surf, 1)

    clear_surf = pygame.image.load("graphics/buttonimages/clear_button.png").convert()
    clear_button = button_classes.Button(50, 700, clear_surf, 1)

    gui_buttons = (exit_button, clear_button)


    name_box = ui_elements.Typing_box(left, start, 600, height, "Fish name")
    min_box = ui_elements.Typing_box(left, start+120, 600, height, "Min weight")
    max_box = ui_elements.Typing_box(left, start+240, 600, height, "Max weight")
    sunny_box = ui_elements.Typing_box(left, start+360, 600, height, "Sunny prob.")
    cloudy_box = ui_elements.Typing_box(left, start+480, 600, height, "Cloudy prob.")

    amount_box = ui_elements.Typing_box(left, start+600, 600, height, "Amount to add")

    boxes = (name_box, min_box, max_box, sunny_box, cloudy_box, amount_box)

    active_box = 0
    boxes[active_box].active = True

    fish_info_dict = {}

    running = True
    message = "all fields not filled"
    
    new_fish_to_add = []

    while running:
        surface.fill("blue")
        surface.blit(helpers.draw_text(message, fonts.title_screen_font, "black"), (30, 900))

        for button in gui_buttons:
            button.draw(surface)

        if exit_button.check_logic() and new_fish_to_add:
            running = False
        if clear_button.check_logic():
            for box in boxes:
                box.written_text = ""
            message = "fields cleared. fill fields with new data"
            


        for i, box in enumerate(boxes):
            if boxes[active_box] is box:
                pygame.draw.rect(surface, "darkgrey", boxes[active_box].outside_box)

            box.draw(surface)

            if box.check_logic(pygame.mouse.get_pressed()[0], surface):

                active_box = i

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    boxes[active_box].written_text = boxes[active_box].written_text[:-1]

                elif event.key == pygame.K_TAB:
                    if active_box == len(boxes)-1:
                        active_box = 0
                    else:
                        active_box += 1

                elif event.key == pygame.K_RETURN:
                    fields_filled = True
                    for box in boxes:
                        if box.written_text == "":
                            fields_filled = False

                    if not fields_filled:
                        message = "fill all fields first!!!"
                    else:
                        klass = globals()[name_box.written_text.capitalize()]
                        for x in range(int(amount_box.written_text)):
                            fish = klass(int(min_box.written_text), int(max_box.written_text), float(sunny_box.written_text), float(cloudy_box.written_text))
                            new_fish_to_add.append(fish)
                        message = f"added new fish. Amount: {amount_box.written_text}"
                        

                else:
                    boxes[active_box].written_text += event.unicode
                


        pygame.display.flip()
 
    new_school = Fish_school(coords, new_fish_to_add)
    helpers.avoid_second_click()
    return new_school







