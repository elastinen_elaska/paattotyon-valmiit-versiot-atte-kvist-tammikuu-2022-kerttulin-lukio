import pygame, pickle, sys, fonts, helpers, functools, music
sys.path.insert(0, "classes")
import ui_elements, button_classes


clock = pygame.time.Clock()

class Record_fish:
    def __init__(self, player_name, fish_name, fish_weight, map_name):
        self.player_name = player_name
        self.fish_name = fish_name
        self.fish_weight = fish_weight
        self.map_name = map_name


def load_records():
    with open("records/biggest_fishes.pickle", "rb") as picklefile:
        return pickle.load(picklefile)


def update_records(fishes, game):
    records = load_records()
    for fish in fishes:
        new = Record_fish(game.player.name, fish.name, fish.weight, game.lake.name)
        records[fish.name.lower()].append(new)
        records[fish.name.lower()] = sorted(records[fish.name.lower()], key=lambda x: x.fish_weight, reverse=True)
        if len(records[fish.name.lower()]) >= 6:
            records[fish.name.lower()] = records[fish.name.lower()][:6]

    save_records(records)


def show_individual_fish(fish, surface):

    exit_img = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()
    exit_button = button_classes.Button(100, 800, exit_img, 1)

    surface.fill("lightblue")
    
    exit_button.draw(surface)


    records = load_records()

    lines = []
    for i, f in enumerate(records[fish]):
        lines.append(f"{i+1}. {f.fish_name} {f.fish_weight} {f.player_name} {f.map_name}")
        surface.blit(helpers.draw_text(lines[i], fonts.medium_font, "black"), (500, 100+i*100))

    pygame.display.flip()

    inspecting = True
    while inspecting:

        if exit_button.check_logic():
            helpers.button_clicked(exit_button, surface)
            inspecting = False


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                inspecting = False

        clock.tick(30)


def draw_records_screen(exit_button, surface, texts):
    surface.fill("lightblue")

    for text in texts:
        text.draw(surface)

    exit_button.draw(surface)

    pygame.display.flip()



def show_records(next_scene, surface):
    # init

    records = load_records()

    texts = []
    for i, fish in enumerate(records):
        texts.append(ui_elements.Clickable_text(500, 100+i*150, 500, 75, 0, f"{fish.capitalize()}", fonts.medium_font))

    exit_img = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()
    exit_button = button_classes.Button(100, 800, exit_img, 1)

    draw_records_screen(exit_button, surface, texts)


    # loop

    watching_records = True
    while watching_records:

        for text in texts:
            if text.check_logic():
                helpers.text_clicked(text, surface)
                show_individual_fish(text.text.lower(), surface)
                for text in texts:
                    text.font_color = text.non_active_font_color
                draw_records_screen(exit_button, surface, texts)



        if exit_button.check_logic():
            helpers.button_clicked(exit_button, surface)
            music.initialize_playing_music(pygame.mixer.music)
            watching_records = False


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                watching_records = False


        clock.tick(30)

    return next_scene



def save_records(new_records):
    with open("records/biggest_fishes.pickle", "wb") as picklefile:
        pickle.dump(new_records, picklefile)

def init_standard_records():
    print("initted")
    records_dict = {
            "ahven": [],
            "hauki": [],
            "lahna": [],
            "kuha": [],
            "särki": []
            }

    save_records(records_dict)



if __name__ == "__main__":
    init_standard_records()






