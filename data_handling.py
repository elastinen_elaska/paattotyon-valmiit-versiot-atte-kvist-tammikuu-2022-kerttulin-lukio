import json, csv, sys, pickle

def save_level_data(lake_name, file_name, data):
    data = tuple(data)
    with open(f"lakes/{lake_name}/{file_name}", "wb") as pickle_file:
        pickle.dump(data, pickle_file)

def load_level_data(lake_name, file_name):
    with open(f"lakes/{lake_name}/data.json") as json_file:
        level_parameters = json.load(json_file)

    world_data = []
    for row in range(level_parameters["world_size_y"] // level_parameters["tile_size"]):
        world_data.append([-1]*(level_parameters["world_size_x"] // level_parameters["tile_size"]))

    save_level_data(lake_name, f"{file_name}", tuple(world_data))


    load_existing_level_data_list(lake_name, file_name)


def load_existing_level_data_tuple(lake_name, file_name):
    with open(f"lakes/{lake_name}/{file_name}", "rb") as pickle_file:
        return tuple(pickle.load(pickle_file))

def load_existing_level_data_list(lake_name, file_name):
    with open(f"lakes/{lake_name}/{file_name}", "rb") as pickle_file:
        return list(pickle.load(pickle_file))


def create_new_level_json_file(lake_name):
    print("File didn't exist, so creating it.")
    world_size_x = int(input("Enter the x-size of the new level: "))
    world_size_y = int(input("Enter the y-size of the new level: "))
    tile_size = int(input("Enter the tile size of the new level: "))
    start_x = int(input("Enter the starting x coordinate of the new level: "))
    start_y = int(input("Enter the starting y coordinate of the new level: "))


    data_for_new_level = {
            "name": lake_name,
            "world_size_x": world_size_x,
            "world_size_y": world_size_y,
            "tile_size": tile_size,
            "starting_point_x": start_x,
            "starting_point_y": start_y
            }

    with open(f"lakes/{lake_name}/data.json", "w+") as jsonfile:
        json.dump(data_for_new_level, jsonfile)

# simply just creates a new empty pickle file
def create_new_fish_pickle_file(lake_name):
    with open(f"lakes/{lake_name}/spring_fish.pickle", "w+") as hello_why_are_you_examining_my_code_hehheh_this_variable_literally_has_no_use:
        print("succesfully created new pickle file SPRIGN")


    with open(f"lakes/{lake_name}/autumn_fish.pickle", "w+") as hello_why_are_you_examining_my_code_hehheh_this_variable_literally_has_no_use:
        print("succesflyy created new pickle file AUTUMN")

def pickle_fish(lake_name, fish_objects_list, season):
    with open(f"lakes/{lake_name}/{season}_fish.pickle", "wb") as pickle_file:
        pickle.dump(fish_objects_list, pickle_file)
    print("pickled fish...")

def load_pickled_fish(lake_name, season):
    fishes = []
    try:
        with open(f"lakes/{lake_name}/{season}_fish.pickle", "rb") as pickle_file:
            fishes = pickle.load(pickle_file)
    except EOFError: # fish pickle file is empty, if this error is raised. Just return an empty list then.
        print("fish pickle file is empty!! Add more fish!")
    return fishes

        










