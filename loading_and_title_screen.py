### This file can only be called from ongintapeli.py so raise exception if its called by itself

if __name__ == "__main__":
    raise Exception("This module shall never be ran as __main__ !!!")

import pygame, time, sys, helpers, fonts, classes, settings_menu, json, random, music, functools

sys.path.insert(0, "classes")

import button_classes, ui_elements

pygame.init()
clock = pygame.time.Clock()



# load images

settings_surface = pygame.image.load("graphics/buttonimages/settings_button.png").convert_alpha()
switch_map_surface = pygame.image.load("graphics/buttonimages/switch_map.png").convert_alpha()

ongelle_image = pygame.image.load("graphics/buttonimages/ongelle.png").convert()
ulos_image = pygame.image.load("graphics/buttonimages/ulos_nappi.png").convert()
back_image = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()

# create buttons

settings_button = button_classes.Button(30, 50, settings_surface, 1)


start_fishing_text = ui_elements.Clickable_text(700, 450, 200, 100, 0, "Kalastamaan!", fonts.medium_font)

switch_button = button_classes.Button(30, 900, switch_map_surface, 1)

ulos_button = button_classes.Button(700, 800, back_image, 1)

back_button = button_classes.Button(30, 50, back_image, 1)

info_surf = pygame.image.load("graphics/buttonimages/info_button.png").convert()
info_button = button_classes.Button(700, 250, info_surf, 1)

gui_button_list = (info_button, settings_button, switch_button, ulos_button)

# create a for loop to create many info button with varying info texts after pressing it




piikkio_text = ui_elements.Clickable_text(1260, 400, 400, 50, 10, "Piikkiönlahti", fonts.title_screen_font)
sattajarvi_text = ui_elements.Clickable_text(1260, 450, 400, 50, 10, "Sattajärvi", fonts.title_screen_font)
kivijarvi_text = ui_elements.Clickable_text(1260, 500, 400, 50, 10, "Kivijärvi", fonts.title_screen_font)
kolkka_text = ui_elements.Clickable_text(1260, 550, 400, 50, 10, "Kolkka", fonts.title_screen_font)

clickable_texts = (piikkio_text, sattajarvi_text, kivijarvi_text, kolkka_text)


spring_text = ui_elements.Clickable_text(690, 600, 100, 80, 10, "Kevät", fonts.title_screen_font)
autumn_text = ui_elements.Clickable_text(690, 680, 100, 80, 10, "Syksy", fonts.title_screen_font)

season_texts = (spring_text, autumn_text)

text = ["\n"]
lake_box = ui_elements.Scrolling_box("darkgrey", "lightgrey", "white", 1260, 400, 500, 500, fonts.title_screen_font, text, 10)


def initialize_info_screen(surface, lake):
    surface.fill("lightblue")     

    surface.blit(helpers.draw_text(f"{lake.capitalize()}", fonts.big_font, "black"), (380, 50))

    back_button.draw(surface)
    pygame.display.flip()


def info_screen(surface, lake): 
    initialize_info_screen(surface, lake)

    with open(f"lakes/{lake}/info.txt") as txtfile:
        data = txtfile.readlines()

    info_box = ui_elements.Scrolling_box((56, 56, 56), (220, 220, 220), "grey", 380, 280, 1200, 650, fonts.title_screen_font, data, 10)

    info_box.draw_text(surface)
    running = True
    while running:
        pos = pygame.mouse.get_pos()
        left_click_pressed = pygame.mouse.get_pressed()[0]

        info_box.check_arrow_boxes_and_scroll_bar(surface, left_click_pressed, pos)
        
        if back_button.check_logic():
            helpers.button_clicked(back_button, surface)
            running = False


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                info_box.check_key_down(surface, event.key)

            if event.type == pygame.MOUSEBUTTONDOWN:
                info_box.check_mouse_scroll(surface, event.button)

            pygame.display.flip()
        clock.tick(30)



# initializes all "basic" things in the window when needed

def initialize_window(lake_name, surface, current_map, current_map_name):
    surface.fill("lightblue")
    surface.blit(helpers.draw_text("Harjoittelu", fonts.start_game_font, "black"), (700, 50))
    surface.blit(helpers.draw_text(f"{current_map_name}:", fonts.title_screen_font, "black"), (30, 180))
    surface.blit(helpers.draw_text(f"Nykyinen vesistö: {lake_name.capitalize()}", fonts.title_screen_font, "black"), (700, 200))
    surface.blit(helpers.draw_text("Valitse vesistö: ", fonts.title_screen_font, "black"), (1250, 200))

    img = pygame.image.load(f"lakes/{lake_name}/{current_map}.png").convert()

    pos = (50, 250)
    
    pygame.draw.rect(surface, "black", (pos[0]-20, pos[1]-20, img.get_width()+40, img.get_height()+40))

    surface.blit(img, pos)

    for gui_button in gui_button_list:
        gui_button.draw(surface)

    lake_box.draw_text(surface)


    for text_button in clickable_texts:
        text_button.draw(surface)

    for button in season_texts:
        button.draw(surface)

    start_fishing_text.font_color = start_fishing_text.non_active_font_color
    start_fishing_text.draw(surface)

    pygame.display.flip()



def put_down_buttons(texts, current):
    for button in texts:
        button.active = False
        button.font_color = button.non_active_font_color
        if button.text.lower() == current:
            button.active = True
            button.font_color = button.active_font_color
    

def practice_screen(surface, clock, music_player):

    user_wants_fps = False

    with open("settings/settings.json") as jsonfile:
        data = json.load(jsonfile)
    volume = data["volume"]

    current_chart = "ground_chart"
    current_chart_name = "Maastokartta"
    current_chosen_lake = "piikkiönlahti"
    current_chosen_lake = "kivijärvi"

    season = "kevät"

    put_down_buttons(clickable_texts, current_chosen_lake)
    put_down_buttons(season_texts, season)


    initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)


    running = True

    while running:

        pos = pygame.mouse.get_pos()

        if info_button.check_logic():

            helpers.button_clicked(info_button, surface)
            info_screen(surface, current_chosen_lake)
            helpers.button_clicked(info_button, surface)
            initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)

        if settings_button.check_logic():
            helpers.button_clicked(settings_button, surface)

            settings_menu.begin(surface, clock, functools.partial(practice_screen, surface, clock, music_player))
            music_player.set_volume(music.load_volume())
            initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)

        if switch_button.check_logic():
            if current_chart == "ground_chart":
                current_chart = "depth_chart"
                current_chart_name = "Syvyyskartta"
            else:
                current_chart = "ground_chart"
                current_chart_name = "Maastokartta"

            helpers.button_clicked(switch_button, surface)
            initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)

        if ulos_button.check_logic():
            helpers.button_clicked(ulos_button, surface)
            # return None because player wants to go back instead of playing
            return None, None
             

        if start_fishing_text.check_logic():
            helpers.text_clicked(start_fishing_text, surface)
            running = False      


        if pygame.mouse.get_pressed()[0] and lake_box.top_arrow.collidepoint(pos[0], pos[1]) and lake_box.scroll > 0:
            lake_box.scroll -= 1
            lake_box.draw_text(surface)
        elif pygame.mouse.get_pressed()[0] and lake_box.bottom_arrow.collidepoint(pos[0], pos[1]) and lake_box.scroll < lake_box.max_scroll:
            lake_box.scroll += 1
            lake_box.draw_text(surface)

        elif pygame.mouse.get_pressed()[0] and lake_box.bar_rect.collidepoint(pos[0], pos[1]):
            lake_box.use_scroll(surface)

        for text_button in clickable_texts:
            if text_button.rect.collidepoint(pos[0], pos[1]) and pygame.mouse.get_pressed()[0]:
                text_button.clicked(surface)
                current_chosen_lake = text_button.text.lower()

                for button in clickable_texts:
                    if button is not text_button:
                        button.active = False
                        button.font_color = button.non_active_font_color

                initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)


        for season_button in season_texts:
            if season_button.rect.collidepoint(pos[0], pos[1]) and pygame.mouse.get_pressed()[0]:
                season_button.clicked(surface)
                season = season_button.text.lower()

                for button in season_texts:
                    if button is not season_button:
                        button.active = False
                        button.font_color = button.non_active_font_color
                initialize_window(current_chosen_lake, surface, current_chart, current_chart_name)


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if pygame.key.name(event.key) == "f8":
                    user_wants_fps = not user_wants_fps
                    pygame.draw.rect(surface, "white", (1599, 99, 300, 100)) 
                if event.key == pygame.K_DOWN and lake_box.scroll < lake_box.max_scroll:
                    lake_box.scroll += 1
                    lake_box.draw_text(surface)
                if event.key == pygame.K_UP and lake_box.scroll > 0:
                    lake_box.scroll -= 1
                    lake_box.draw_text(surface)


        if user_wants_fps:
            surface.blit(helpers.draw_fps_text(surface, clock), (1600, 100))

        pygame.display.flip()
        pygame.time.wait(5)
        clock.tick(30)


    season_dict = {
            "kevät": "spring",
            "syksy": "autumn"
            }


    return current_chosen_lake, season_dict[season]


