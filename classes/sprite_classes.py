
import pygame




class World_object(pygame.sprite.Sprite):
    def __init__(self, x_cor, y_cor, image):
        pygame.sprite.Sprite.__init__(self)
        self.x_cor = x_cor
        self.y_cor = y_cor
        self.image = pygame.image.load(f"graphics/spriteimages/{image}").convert_alpha()

        self.rect = self.image.get_rect()

    def set_rect(self):
        self.rect.x = self.x_cor
        self.rect.y = self.y_cor


class Animated_sprite(pygame.sprite.Sprite):
    def __init__(self, x_cor, y_cor, path_to_file, frames, frame_amount):
        pygame.sprite.Sprite.__init__(self)
        self.x_cor = x_cor
        self.y_cor = y_cor

        self.frame = 0
        self.images = []
        self.path_to_file = path_to_file
        self.frames = frames
        self.frame_amount = frame_amount

        for i in range(1, frame_amount+1):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            for j in range(frames):
                self.images.append(image)
        for i in range(frame_amount, 1, -1):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            for j in range(frames):
                self.images.append(image)


        self.image = self.images[self.frame]
        self.rect = self.image.get_rect()

    def set_rect(self):
        self.rect.x = self.x_cor
        self.rect.y = self.y_cor





