import pygame, random

class Pseudo_fish:
    def __init__(self, weight, name):
        self.weight = weight
        self.name = name

    def __str__(self):
        return f"{self.name.capitalize()} {self.weight} g"


class Fish:
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        self.min_size = min_size
        self.max_size = max_size
        self.sunny_probability = sunny_probability
        self.cloudy_probability = cloudy_probability

        self.weight = 9999999999
        self.biting_time = 9999999999

    def __str__(self):
        return f"{self.name.capitalize()}  {self.weight} g"

        

class Fish_school:
    def __init__(self, coordinates, members):
        self.coordinates = coordinates
        self.members = members



class Perch(Fish):
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        super().__init__(min_size, max_size, sunny_probability, cloudy_probability)
        self.image = "graphics/fish_images/perch_images/ahven.png"
        self.name = "ahven"

    def test_eating(self, fishing_info):

        eating_prob = {
                "sunny": self.sunny_probability,
                "cloudy": self.cloudy_probability
                }

        
        num = random.random()
        prob = 0

        prob += eating_prob[fishing_info.sky]
        return num < prob ## true if num(random float) is smaller than probability 

    def set_biting_time(self, fishing_info):
        self.sunny_eating_time = 2
        self.cloudy_eating_time = 2

        self.worm_bait_time = 1
        self.fish_bait_time = 3

        eating_times = {
                "sunny": self.sunny_eating_time,
                "cloudy": self.cloudy_eating_time
                }
        bait_times = {
                "worm_bait": self.worm_bait_time,
                "fish_bait": self.fish_bait_time 
                }

        time = eating_times[fishing_info.sky]
        time += bait_times[fishing_info.bait.name]

        if fishing_info.bait.name ==  "fish_bait":
            if self.weight < 100: ## if fish is max 100 g it doesn't eat fish bait
                return 0
            elif self.weight < 999: ## if fish is over 999 g bait doesn't matter, but for smaller fish biterate goes down
                time += 5 - self.max_size % 5

        self.biting_time = time

        return time
    
    def set_weight(self):
        self.weight = random.randint(self.min_size, self.max_size)

    def check_weight(self, info):
        weights = {
                "worm_bait": 0,
                "fish_bait": 50
                }
        self.weight += weights[info.bait.name]

    def gets_away(self):
        return random.randint(1, 15) == 1



class Pike(Fish):
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        super().__init__(min_size, max_size, sunny_probability, cloudy_probability)
        self.image = "graphics/fish_images/pike_images/pike.png"
        self.name = "hauki"


    def test_eating(self, fishing_info):
        eating_prob = {
                "sunny": self.sunny_probability,
                "cloudy": self.cloudy_probability
                }

        
        num = random.random()
        prob = 0

        prob += eating_prob[fishing_info.sky]
        return num < prob ## true if num(random float) is smaller than probability 


    def set_biting_time(self, fishing_info):
        self.sunny_eating_time = 4
        self.cloudy_eating_time = 4

        self.worm_bait_time = 10
        self.fish_bait_time = 4

        eating_times = {
                "sunny": self.sunny_eating_time,
                "cloudy": self.cloudy_eating_time
                }
        bait_times = {
                "worm_bait": self.worm_bait_time,
                "fish_bait": self.fish_bait_time 
                }

        time = eating_times[fishing_info.sky]
        time += bait_times[fishing_info.bait.name]

        if fishing_info.bait.name ==  "worm_bait":
            if self.weight > 800: # if fish is over 800 it doesn't eat worms
                return 0

        self.biting_time = time

        return time
    




    def set_weight(self):
        self.weight = random.randint(self.min_size, self.max_size)

    def check_weight(self, info):
        return

    def gets_away(self):
        return random.randint(1, 10) == 2


class Roach(Fish):
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        super().__init__(min_size, max_size, sunny_probability, cloudy_probability)
        self.image = "graphics/fish_images/roach_images/roach.png"
        self.name = "särki"


    def test_eating(self, fishing_info):
        eating_prob = {
                "sunny": self.sunny_probability,
                "cloudy": self.cloudy_probability
                }

        
        num = random.random()
        prob = 0

        prob += eating_prob[fishing_info.sky]
        return num < prob ## true if num(random float) is smaller than probability 


    def set_biting_time(self, fishing_info):
        self.sunny_eating_time = 1
        self.cloudy_eating_time = 3

        self.worm_bait_time = 1
        self.fish_bait_time = 40000

        eating_times = {
                "sunny": self.sunny_eating_time,
                "cloudy": self.cloudy_eating_time
                }
        bait_times = {
                "worm_bait": self.worm_bait_time,
                "fish_bait": self.fish_bait_time 
                }

        time = eating_times[fishing_info.sky]
        time += bait_times[fishing_info.bait.name]

        if fishing_info.bait.name ==  "fish_bait":
            return 0 # roach doesn't eat itself....

        self.biting_time = time

        return time
    




    def set_weight(self):
        self.weight = random.randint(self.min_size, self.max_size)

    def check_weight(self, info):
        return


    def gets_away(self):
        return random.randint(1, 15) == 1


class Bream(Fish):
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        super().__init__(min_size, max_size, sunny_probability, cloudy_probability)
        self.image = "graphics/fish_images/bream_images/lahna.png"
        self.name = "lahna"


    def test_eating(self, fishing_info):
        eating_prob = {
                "sunny": self.sunny_probability,
                "cloudy": self.cloudy_probability
                }

        
        num = random.random()
        prob = 0

        prob += eating_prob[fishing_info.sky]
        return num < prob ## true if num(random float) is smaller than probability 


    def set_biting_time(self, fishing_info):
        self.sunny_eating_time = 1
        self.cloudy_eating_time = 5

        self.worm_bait_time = 1
        self.fish_bait_time = 40000

        eating_times = {
                "sunny": self.sunny_eating_time,
                "cloudy": self.cloudy_eating_time
                }
        bait_times = {
                "worm_bait": self.worm_bait_time,
                "fish_bait": self.fish_bait_time 
                }

        time = eating_times[fishing_info.sky]
        time += bait_times[fishing_info.bait.name]

        if fishing_info.bait.name ==  "fish_bait":
            return 0 # roach doesn't eat itself....

        self.biting_time = time

        return time
    




    def set_weight(self):
        self.weight = random.randint(self.min_size, self.max_size)

    def check_weight(self, info):
        return


    def gets_away(self):
        return random.randint(1, 8) == 1


class Zander(Fish):
    def __init__(self, min_size, max_size, sunny_probability, cloudy_probability):
        super().__init__(min_size, max_size, sunny_probability, cloudy_probability)
        self.image = "graphics/fish_images/zander_images/kuha.png"
        self.name = "kuha"


    def test_eating(self, fishing_info):
        eating_prob = {
                "sunny": self.sunny_probability,
                "cloudy": self.cloudy_probability
                }

        
        num = random.random()
        prob = 0

        prob += eating_prob[fishing_info.sky]
        return num < prob ## true if num(random float) is smaller than probability 


    def set_biting_time(self, fishing_info):
        self.sunny_eating_time = 3
        self.cloudy_eating_time = 1

        self.worm_bait_time = 2
        self.fish_bait_time = 2

        eating_times = {
                "sunny": self.sunny_eating_time,
                "cloudy": self.cloudy_eating_time
                }
        bait_times = {
                "worm_bait": self.worm_bait_time,
                "fish_bait": self.fish_bait_time 
                }

        time = eating_times[fishing_info.sky]
        time += bait_times[fishing_info.bait.name]

        self.biting_time = time

        return time
    




    def set_weight(self):
        self.weight = random.randint(self.min_size, self.max_size)

    def check_weight(self, info):
        return


    def gets_away(self):
        return random.randint(1, 10) == 1


class Fishing_info:
    def __init__(self, x_cor, y_cor, bait, sky):
        self.x_cor = x_cor
        self.y_cor = y_cor
        self.bait = bait
        self.sky = sky

        imgs = {
                "sunny": pygame.image.load("graphics/spriteimages/sun.png").convert_alpha(),
                "cloudy": pygame.image.load("graphics/spriteimages/clouds.png").convert_alpha()
                }

        self.sky_img = imgs[self.sky]

    def update_x_cor(self, x):
        self.x_cor += x

    def update_y_cor(self, y):
        self.y_cor += y





