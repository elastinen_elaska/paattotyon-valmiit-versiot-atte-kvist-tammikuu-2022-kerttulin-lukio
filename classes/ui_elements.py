import pygame, helpers, fonts, sys

class Slider:
    def __init__(self, color, x, y, width, height, circle_color, active_color, current_percentage):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.circle_color = circle_color
        self.active_color = active_color
        self.radius = min(self.height, self.width)//2


class Slider_horizontal(Slider):

    def __init__(self, color, x, y, width, height, circle_color, active_color, current_percentage):
        super().__init__(color, x, y, width, height, circle_color, active_color, current_percentage)

        self.left_border = self.x+self.radius
        self.right_border = self.x+self.width-self.radius

        self.center = [self.left_border+(self.right_border-self.left_border)*current_percentage, self.y+self.radius]

        self.rect = pygame.Rect(self.x+self.radius, self.y+self.radius, self.width-self.radius, self.height-self.radius)

        self.currently_pressed = False

    def collides_with_ball(self, x, y):
        return (abs(self.center[0]-x) <= self.radius and abs(self.center[1]-y) <= self.radius)


    def draw(self, surface):
        pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), border_radius=self.radius)
        color = self.circle_color

        if self.currently_pressed:

            pos = pygame.mouse.get_pos()
            color = self.active_color

            pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), border_radius=self.radius)

            # calculate position where ball goes
            if pos[0] >= self.left_border and pos[0]<=self.right_border:
                self.center[0] = pos[0]
            elif pos[0] <= self.left_border:
                self.center[0] = self.x+self.radius
            else:
                self.center[0] = self.x+self.width-self.radius



        pygame.draw.circle(surface, color, self.center, self.radius)

        return round((self.center[0]-self.left_border)/(self.right_border-self.left_border), 2)

class Slider_vertical(Slider):
    def __init__(self, color, x, y, width, height, circle_color, active_color, current_percentage):
        super().__init__(color, x, y, width, height, circle_color, active_color, current_percentage)

        self.top_border = self.y+self.radius
        self.bottom_border = self.y+self.height-self.radius

        self.center = [self.x+self.radius, self.top_border+(self.bottom_border-self.top_border)*current_percentage]
        self.active_color = self.color
        self.active = False

    def draw(self, surface):
        pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), border_radius=(min(self.width, self.height)//2))
        pos = pygame.mouse.get_pos()
        color = self.circle_color

        # check if mouse is inside circle
        if abs(pos[0]-self.center[0]) < self.radius and abs(pos[1]-self.center[1]) < self.radius:
            color = self.active_color
            pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), border_radius=(min(self.width, self.height)//2))
            if pygame.mouse.get_pressed()[0]:
                if pos[1] >= self.top_border and pos[1]<=self.bottom_border:
                    self.center[1] = pos[1]

        pygame.draw.circle(surface, color, self.center, self.radius)

        return round((self.center[1]-self.top_border)/(self.bottom_border-self.top_border), 2)

class Fishing_bar(Slider_vertical):
    def __init__(self, color, x, y, width, height, circle_color, active_color, current_percentage):
        super().__init__(color, x, y, width, height, circle_color, active_color, current_percentage)
        self.current_circle_color = self.color
        self.active = False

    
    def draw(self, surface):
        pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), border_radius=(min(self.width, self.height)//2))

        pygame.draw.circle(surface, self.current_circle_color, self.center, self.radius)

        pygame.draw.line(surface, "black", (self.x, self.top_border+(self.bottom_border-self.top_border)*(self.depth/10)+self.radius), (self.x+self.width, self.top_border+(self.bottom_border-self.top_border)*(self.depth/10)+self.radius), width=5)

    def move(self, y):
        self.center[1] += y

    def set_active(self):
        if self.active:
            self.current_circle_color = self.color
        if not self.active:
            self.current_circle_color = self.circle_color

        self.active = not self.active

    def set_depth(self, depth):
        self.depth = depth
        self.center[1] = self.top_border+(self.bottom_border-self.top_border)*(self.depth/10)


class Typing_box:
    def __init__(self, x, y, width, height, display_text):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.display_text = display_text

        self.written_text = ""

        self.active = False

        self.color = "lightgrey"

        border = 10
        self.outside_box = (x-border, y-border, width+2*border, height+2*border)
        self.inside_box = (x, y, width, height)

        self.text_pos = (x+border, y+border)
        self.font = pygame.font.Font("SHPinscher-Regular.otf", height-2*border)

    def draw(self, surface, add_to_end=""):
        pygame.draw.rect(surface, "lightgrey", self.inside_box)

        if self.written_text == "":
            self.draw_text(self.display_text, self.font, surface)
        else:
            self.draw_text(self.written_text + add_to_end, self.font, surface)

    def draw_text(self, text, font, surface):
        img = font.render(text, True, "black")
        surface.blit(img, self.text_pos)


    def check_logic(self, mouse_pressed, surface):

        pos = pygame.mouse.get_pos()

        if pos[0] > self.x and pos[0] < self.x+self.width and pos[1] > self.y and pos[1] < self.y+self.height and mouse_pressed:
            self.active = True
            return True
        elif mouse_pressed:
            self.active = False
            return False


class Scrolling_box:
    def __init__(self, outside_color, inside_color, bar_color, x, y, x_size, y_size, font, text, row_space):
        self.outside_color = outside_color
        self.inside_color = inside_color
        self.non_active_bar_color = bar_color
        self.active_bar_color = "black"
        self.bar_color = self.non_active_bar_color
        self.x = x
        self.y = y
        self.x_size = x_size
        self.y_size = y_size
        self.font = font
        self.text = text

        self.correct_text = self.cut_rows_to_correct_size(self.text) # correctly cuts own text into shorter rows


        self.row_space = row_space

        self.scroll = 0

        self.line_amount_per_screen = self.y_size // (fonts.get_height(self.font) + self.row_space)

        self.total_lines = self.get_total_lines(self.text)

        self.max_scroll = self.total_lines-self.line_amount_per_screen

        if self.max_scroll >= 1:
            self.bar_length = self.y_size / self.max_scroll
            self.down_jump = (self.y_size-self.bar_length-40) / self.max_scroll
        else:
            self.bar_length = self.y_size-40
            self.down_jump = 0



        self.top_arrow = pygame.Rect(self.x_size-20+self.x, self.y, 20, 20)
        self.bottom_arrow = pygame.Rect(self.x_size-20+self.x, self.y+self.y_size-20, 20, 20)

        self.bar_rect = pygame.Rect(self.x+self.x_size-20, self.y+20+self.scroll*self.down_jump, 20, self.bar_length)
        self.height = fonts.get_height(self.font)+self.row_space

        self.outside_box = pygame.Rect(self.x-10, self.y-10, self.x_size+20, self.y_size+20)
        self.inside_box = pygame.Rect(self.x, self.y, self.x_size, self.y_size)



    def get_lines_from_row(self, row):
        indexes = [0]
        
        i = 0
        line = ""
        while True:
            letter = row[i]
            if fonts.get_width(self.font, line) > self.x_size-80:
                indexes.append(i)
                line = ""
            if letter == "\n":
                break

            line += row[i]
            i += 1

        indexes.append(None)
        if len(indexes) == 1:
            return line

        return [row[indexes[i]:indexes[i+1]] for i in range(len(indexes)-1)] 

    def get_total_lines(self, text):
        lines = []
        total = 0
        for line in text:
            rows = self.get_lines_from_row(line)
            for x in rows:
                total += 1
        return total
        

    def cut_rows_to_correct_size(self, text):
        rows = []
        for row in text:
            lines = self.get_lines_from_row(row)
            for line in lines:
                rows.append(line)
               
        rows = [row.strip() for row in rows]
        return rows

    def draw(self, surface):
        # update bar rect to correct position if scroll value changed
        self.bar_rect = pygame.Rect(self.x+self.x_size-20, self.y+20+self.scroll*self.down_jump, 20, self.bar_length)

        pygame.draw.rect(surface, self.outside_color, self.outside_box)
        pygame.draw.rect(surface, self.inside_color, self.inside_box)
        pygame.draw.rect(surface, (230, 230, 230), (self.x_size-20+self.x, self.y+20, 20, self.y_size-20))
        pygame.draw.rect(surface, "black", self.top_arrow)
        pygame.draw.rect(surface, "black", self.bottom_arrow)
        pygame.draw.line(surface, "white", (self.x_size-20+self.x, self.y+20), (self.x+self.x_size-10, self.y))
        pygame.draw.line(surface, "white", (self.x+self.x_size-10, self.y), (self.x_size+self.x, self.y+20))
        pygame.draw.line(surface, "white", (self.x+self.x_size-20, self.y+self.y_size-20), (self.x+self.x_size-10, self.y+self.y_size))
        pygame.draw.line(surface, "white", (self.x+self.x_size-10, self.y+self.y_size), (self.x+self.x_size, self.y+self.y_size-20))
        pygame.draw.rect(surface, self.bar_color, self.bar_rect)

    def draw_text(self, surface):
        self.draw(surface)
        for x, row in enumerate(self.correct_text[self.scroll:self.scroll+self.line_amount_per_screen]):
            surface.blit(helpers.draw_text(row, self.font, "black"), (self.x+20, self.y+20+x*self.height))

    def use_scroll(self, surface):
        start_y = pygame.mouse.get_pos()[1]
    
        self.bar_color = self.active_bar_color
        self.draw_text(surface)
        while True:
            pygame.event.pump()
            current_y = pygame.mouse.get_pos()[1]
            clicking = pygame.mouse.get_pressed()[0]
            if not clicking: break

            if current_y - start_y > self.down_jump and self.scroll < self.max_scroll:
                start_y = current_y
                self.scroll += 1
                self.draw_text(surface)
            elif start_y - current_y > self.down_jump and self.scroll > 0:
                start_y = current_y
                self.scroll -= 1
                self.draw_text(surface)

            pygame.display.flip()

        self.bar_color = self.non_active_bar_color
        self.draw_text(surface)


    def check_arrow_boxes_and_scroll_bar(self, surface, left_click_pressed, pos):
        if left_click_pressed and self.top_arrow.collidepoint(pos[0], pos[1]) and self.scroll > 0:
            self.scroll -= 1
            self.draw_text(surface)
            pygame.display.flip()

        elif left_click_pressed and self.bottom_arrow.collidepoint(pos[0], pos[1]) and self.scroll < self.max_scroll:
            self.scroll += 1
            self.draw_text(surface)
            pygame.display.flip()

        elif left_click_pressed and self.bar_rect.collidepoint(pos[0], pos[1]):
            self.use_scroll(surface)
            pygame.display.flip()

    def check_key_down(self, surface, key):
        if key == pygame.K_DOWN and self.scroll < self.max_scroll:
            self.scroll += 1
            self.draw_text(surface)
        elif key == pygame.K_UP and self.scroll > 0:
            self.scroll -= 1
            self.draw_text(surface)

    def check_mouse_scroll(self, surface, mouse_button):
        if mouse_button == 4 and self.scroll > 0:
            self.scroll -= 1
            self.draw_text(surface)
        elif mouse_button == 5 and self.scroll < self.max_scroll:
            self.scroll += 1
            self.draw_text(surface)


    

class Clickable_text:
    def __init__(self, x, y, x_size, y_size, margin, text, font):
        self.x = x
        self.y = y
        self.rect = pygame.Rect(x, y, x_size, y_size)
        self.margin = margin
        self.text = text
        self.font = font

        self.active_font_color = "red"
        self.non_active_font_color = "white"
        self.font_color = self.non_active_font_color

        self.active = False

    def draw(self, surface):
        surface.blit(helpers.draw_text(self.text, self.font, self.font_color), (self.x+self.margin, self.y+self.margin))

    def clicked(self, surface):
        self.font_color = self.active_font_color
        self.draw(surface)
        pygame.display.flip()
        while True:
            pygame.event.pump()
            clicked = pygame.mouse.get_pressed()[0]

            if not clicked:
                break


        self.active = True

    def check_logic(self):
        pos = pygame.mouse.get_pos()
        return self.rect.collidepoint(pos) and pygame.mouse.get_pressed()[0]






