import pygame

class Button:
    def __init__(self, x, y, image, scale):
        width = image.get_width()
        height = image.get_height()
        self.image = pygame.transform.scale(image, (int(width*scale), int(height*scale)))
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.clicked = False


    def get_rect(self):
        return (self.rect.x, self.rect.y, self.image.get_width(), self.image.get_height())

    # this function check_logics itself (a button).
    # acts as button: when this image is clicked it return "action" which mean
    # whether the button was clicked. 

    def check_logic(self):
        action = False
        pos = pygame.mouse.get_pos()

        if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False and pos[0] > self.rect.x and pos[0] < self.rect.x+self.image.get_width() and pos[1] > self.rect.y and pos[1] < self.rect.y+self.image.get_height():
            action = True
            self.clicked = True

        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False


        return action 


    def draw(self, surface):
        surface.blit(self.image, (self.rect.x, self.rect.y))

    def draw_clicked_state(self, surface):
        pygame.draw.rect(surface, "red", (self.rect.x-10, self.rect.y-10, self.image.get_width()+20, self.image.get_height()+20))
        self.draw(surface)



class Bait_box_button(Button):
    def __init__(self, x, y, image, scale, bait_id):
        super().__init__(x, y, image, scale)
        self.bait_id = bait_id

class Button_with_function(Button):
    def __init__(self, x, y, image, scale, function):
        super().__init__(x, y, image, scale)
        self.function = function

    def return_function(self):
        return self.function

class Menu_button:
    def __init__(self, x, y, image):
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.clicked = False

    def check_logic(self):
        active = False
        pos = pygame.mouse.get_pos()

        #check mouse over and clicked condition
        if self.rect.collidepoint(pos) and pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
            self.clicked = True
            active = True

        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False

        return active


    def draw(self, surface):
        surface.blit(self.image, (self.rect.x, self.rect.y))

    def draw_clicked_state(self, surface):
        pygame.draw.rect(surface, "red", (self.rect.x-10, self.rect.y-10, self.image.get_width()+20, self.image.get_height()+20))
        self.draw(surface)




class Lake_button:
    def __init__(self, x, y, image, file_to_active):
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)
        self.clicked = False
        self.file_to_active = file_to_active

    def check_logic(self):
        active = False
        pos = pygame.mouse.get_pos()

        #check mouse over and clicked condition
        if self.rect.collidepoint(pos) and pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
            self.clicked = True
            active = True

        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False

        return active

    def draw(self, surface):
        surface.blit(self.image, (self.rect.x, self.rect.y))

    def draw_clicked_state(self, surface):
        pygame.draw.rect(surface, "red", (self.rect.x-10, self.rect.y-10, self.image.get_width()+20, self.image.get_height()+20))
        self.draw(surface)




class Drawing_data_in_editor:
    def __init__ (self, data_to_use, file_name_to_save, tiles_to_draw, pictures_list, buttons_list):
        self.data_to_use = data_to_use
        self.file_name_to_save = file_name_to_save
        self.tiles_to_draw = tiles_to_draw
        self.pictures_list = pictures_list
        self.buttons_list = buttons_list























