import pygame  ## pygame has to be the very first module to be imported
pygame.init() ## and these settings need to initialized before importing other modules

                # very beginning things pygame requires
screen_width = 1920
screen_height = 1080
screen = pygame.display.set_mode((screen_width, screen_height))
screen.set_alpha(None)
pygame.display.set_caption("Ongintapeli / ICT-päättötyö, Atte Kvist, tammikuu 2022")

pygame.time.set_timer(pygame.USEREVENT, 1000)

# this event can be used for anything that requires something to happen between 50 milliseconds
FISH_MOVES_EVENT = pygame.USEREVENT + 1
pygame.time.set_timer(FISH_MOVES_EVENT, 50)

BOBBER_UP_AND_DOWN_EVENT = pygame.USEREVENT + 2
pygame.time.set_timer(BOBBER_UP_AND_DOWN_EVENT, 100)

MOVE_BOBBER_EVENT = pygame.USEREVENT + 3
pygame.time.set_timer(MOVE_BOBBER_EVENT, 500)

CHANGE_DIRECTION_EVENT = pygame.USEREVENT + 4

BOT_MOVES_EVENT = pygame.USEREVENT + 5
#pygame.time.set_timer(BOT_MOVES_EVENT, 60000)
pygame.time.set_timer(BOT_MOVES_EVENT, 30000)

UPDATE_BOTS_EVENT = pygame.USEREVENT + 6
pygame.time.set_timer(UPDATE_BOTS_EVENT, 100)

FLASH_UNDERSCORE_EVENT = pygame.USEREVENT + 7
pygame.time.set_timer(FLASH_UNDERSCORE_EVENT, 150)

pygame.event.set_allowed((pygame.QUIT, pygame.KEYDOWN, pygame.USEREVENT, FISH_MOVES_EVENT, BOBBER_UP_AND_DOWN_EVENT)) # set allowed events to improve performance

clock = pygame.time.Clock()

# these functions are used at the very beginning to load and import and initialize everything

def draw_message(message, loading_screen_surface):
    screen.blit(loading_screen_surface, (0, 0))
    screen.blit(message, (800, 950))
    pygame.display.flip()
    #pygame.time.wait(80)


def loading_animation():
    global sys, pickle, csv, random, json, time, functools
    global matplotlib
    global fonts, loading_and_title_screen, helpers, world_drawing, data_handling, ui_elements, music, settings_menu, records
    global fish_info_classes, button_classes, sprite_classes



    messages = ("lisätään sys moduuli", "lisätään standardimoduulit", "lisätään ulkoiset kirjastot", "lisätään oma tiedostot ja kirjasto", "lisätään omat alihakemistojen tiedostot", "asetukset...")

    loading_screen_font = pygame.font.Font("SHPinscher-Regular.otf", 30)
    messages = tuple(loading_screen_font.render(message, True, "white") for message in messages)



    loading_screen_surface = pygame.image.load("graphics/reallifeimages/loading_screen_picture.png").convert_alpha()

    draw_message(messages[0], loading_screen_surface)
    import sys
    
    sys.path.insert(0, "classes") # import classes directory 

    draw_message(messages[1], loading_screen_surface)
    import pickle, csv, random, json, time, functools # standard modules 

    draw_message(messages[2], loading_screen_surface)
    import matplotlib # external libraries

    draw_message(messages[3], loading_screen_surface)
    import records, fonts, loading_and_title_screen, helpers, world_drawing, data_handling, ui_elements, music # own files

    draw_message(messages[4], loading_screen_surface)
    import fish_info_classes, button_classes, sprite_classes # import own files from "classes" subdirectory

    draw_message(messages[5], loading_screen_surface)
    import settings_menu

    loading_screen_text = fonts.loading_screen_font.render("Klikkaa aloittaaksesi.", False, "white")
    screen.blit(loading_screen_surface, (0, 0))


def loading_screen():

    game_over = False

    loading_screen_text = fonts.loading_screen_font.render("Klikkaa jatkaaksesi.", True, "white")
    screen.blit(loading_screen_text, (800, 950, 100, 400))
    pygame.display.flip()

    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                game_over = True
            
        clock.tick(30)

    helpers.avoid_second_click()


import music

def init_starting_music():
    pygame.mixer.music.load("music/short_clips/Tervetuloa_vesille.wav")
    pygame.mixer.music.set_volume(music.load_volume())
    pygame.mixer.music.play(loops=1)
    pygame.event.pump()

init_starting_music()
loading_animation()
loading_screen()


## Classes here

class Player(pygame.sprite.Sprite):
    def __init__(self, path_to_file):
        pygame.sprite.Sprite.__init__(self)

        self.frame = 0
        self.images = []
        self.path_to_file = path_to_file

        animation_speed = 3
        for i in range(1, 9):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            for i in range(animation_speed): # animation speed for change (currently 5), lower numbers means faster animation
                self.images.append(image)

        # adds in reverse all the images so animation doesn't do a sudden jump
        for i in range(8, 1, -1):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            self.images.append(image)

        self.image = self.images[self.frame]
        self.rect = self.image.get_rect()

        self.fish = []

        self.name = entire_program_state.player_name


    def init_self(self, game):
        self.current_row = (self.rect.y+game.scroll_y) // game.lake.TILE_SIZE
        self.current_column = (self.rect.x + game.scroll_x) // game.lake.TILE_SIZE

        self.tile_below = game.lake.over_world[self.current_row+1][self.current_column]
        self.tile_above = game.lake.over_world[self.current_row-1][self.current_column]
        self.tile_on_left = game.lake.over_world[self.current_row][self.current_column-1]
        self.tile_on_right = game.lake.over_world[self.current_row][self.current_column+1]

    def set_new_values(self, game):
        self.current_row = (self.rect.y+game.scroll_y) // game.lake.TILE_SIZE
        self.current_column = (self.rect.x + game.scroll_x) // game.lake.TILE_SIZE

        self.tile_above = game.lake.over_world[self.current_row-1][self.current_column]
        self.tile_on_left = game.lake.over_world[self.current_row][self.current_column-1]
        self.tile_on_right = game.lake.over_world[self.current_row][self.current_column+1]
        self.tile_below = game.lake.over_world[self.current_row+1][self.current_column]
        update_animation(self)
 
    def move_up(self, game):
        if self.rect.y == half_height and game.scroll_y > 0 and self.tile_above in tiles_can_go_through:
            self.set_new_values(game)
            return speed

        elif self.tile_above in tiles_can_go_through and self.current_row >= 0:
            self.set_new_values(game)
            self.rect.y -= speed

        return 0

    def move_down(self, game):

        if self.rect.y == half_height and game.scroll_y < game.lake.world_size_y-screen_height and self.tile_below in tiles_can_go_through:
            self.set_new_values(game)
            return speed

        elif self.tile_below in tiles_can_go_through and self.current_row < game.lake.tiles_in_y-1:
            self.set_new_values(game)
            self.rect.y += speed

        return 0


    def move_left(self, game):
        if self.rect.x == half_width and game.scroll_x > 0 and self.tile_on_left in tiles_can_go_through:
            self.set_new_values(game)
            return speed

        elif self.tile_on_left in tiles_can_go_through and self.current_column >= 0:
            self.set_new_values(game)
            self.rect.x -= speed

        return 0

    def move_right(self, game):

        if self.rect.x == half_width and game.scroll_x < game.lake.world_size_x-screen_width and self.tile_on_right in tiles_can_go_through:
            self.set_new_values(game)
            return speed

        elif self.tile_on_right in tiles_can_go_through and self.current_column < game.lake.tiles_in_x-1:
            self.set_new_values(game)
            self.rect.x += speed

        return 0


    def set_rect(self):
        self.rect.x = self.x_cor
        self.rect.y = self.y_cor


class Lake:
    def __init__(self, name):
        self.name = name

        with open(f"lakes/{name}/data.json") as file:
            parameters = json.load(file)

        self.TILE_SIZE = parameters["tile_size"]
        self.world_size_x = parameters["world_size_x"]
        self.world_size_y = parameters["world_size_y"]

        self.start_x = parameters["starting_point_x"]
        self.start_y = parameters["starting_point_y"]

        self.over_world = data_handling.load_existing_level_data_tuple(name, "draw_data.pickle")
        self.underwater = data_handling.load_existing_level_data_tuple(name, "underwater_data.pickle")

        self.tiles_in_x = len(self.over_world[0]) -1
        self.tiles_in_y = len(self.over_world) -1


        self.fish_schools = []

        self.depth_chart = pygame.image.load(f"lakes/{self.name}/depth_chart.png").convert_alpha()
        self.ground_chart = pygame.image.load(f"lakes/{self.name}/ground_chart.png").convert_alpha()
        

class Bot(pygame.sprite.Sprite):
    def __init__(self, path_to_file, name):
        pygame.sprite.Sprite.__init__(self)

        self.frame = 0
        self.images = []
        self.path_to_file = path_to_file

        self.name = name

        animation_speed = 3
        for i in range(1, 9):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            for i in range(animation_speed): # animation speed for change (currently 5), lower numbers means faster animation
                self.images.append(image)

        # adds in reverse all the images so animation doesn't do a sudden jump
        for i in range(8, 1, -1):
            image = pygame.image.load(f"{path_to_file}" + str(i) + ".png").convert_alpha()
            self.images.append(image)

        self.image = self.images[self.frame]
        self.rect = self.image.get_rect()

        self.fish = []



    def init_self(self, game):
        self.current_row = (self.rect.y+game.scroll_y) // game.lake.TILE_SIZE
        self.current_column = (self.rect.x + game.scroll_x) // game.lake.TILE_SIZE

        self.tile_below = game.lake.over_world[self.current_row+1][self.current_column]
        self.tile_above = game.lake.over_world[self.current_row-1][self.current_column]
        self.tile_on_left = game.lake.over_world[self.current_row][self.current_column-1]
        self.tile_on_right = game.lake.over_world[self.current_row][self.current_column+1]

    def set_new_values(self, game):
        self.current_row = self.world_y // game.lake.TILE_SIZE
        self.current_column = self.world_x // game.lake.TILE_SIZE

        self.tile_above = game.lake.over_world[self.current_row-1][self.current_column]
        self.tile_on_left = game.lake.over_world[self.current_row][self.current_column-1]
        self.tile_on_right = game.lake.over_world[self.current_row][self.current_column+1]
        self.tile_below = game.lake.over_world[self.current_row+1][self.current_column]
 

    def set_rect(self):
        self.rect.x = self.x_cor
        self.rect.y = self.y_cor


    def move(self, pos):
        update_animation(self)
        if pos[0] > self.world_x:
            self.rect.x += 1
            self.world_x += 1
        elif pos[0] < self.world_x:
            self.rect.x -= 1
            self.world_x -= 1
        else:
            self.moves_in_x = False

        if pos[1] > self.world_y:
            self.rect.y += 1
            self.world_y += 1
        elif pos[1] < self.world_y:
            self.rect.y -= 1
            self.world_y -= 1
        else:
            self.moves_in_y = False



class Lake:
    def __init__(self, name):
        self.name = name

        with open(f"lakes/{name}/data.json") as file:
            parameters = json.load(file)

        self.TILE_SIZE = parameters["tile_size"]
        self.world_size_x = parameters["world_size_x"]
        self.world_size_y = parameters["world_size_y"]

        self.start_x = parameters["starting_point_x"]
        self.start_y = parameters["starting_point_y"]

        self.over_world = data_handling.load_existing_level_data_tuple(name, "draw_data.pickle")
        self.underwater = data_handling.load_existing_level_data_tuple(name, "underwater_data.pickle")

        self.tiles_in_x = len(self.over_world[0]) -1
        self.tiles_in_y = len(self.over_world) -1


        self.fish_schools = []

        self.depth_chart = pygame.image.load(f"lakes/{self.name}/depth_chart.png").convert_alpha()
        self.ground_chart = pygame.image.load(f"lakes/{self.name}/ground_chart.png").convert_alpha()
        


class Game:
    def __init__(self, duration, lake_name, season, scene_after_game):
        self.fps = entire_program_state.fps
        self.duration = duration
        self.season = season
        self.scene_after_game = scene_after_game

        self.player_list = pygame.sprite.Group()
        self.bot_list = pygame.sprite.Group()
        self.init(lake_name)

    def handle_time(self):
        self.duration -= 1
        self.draw_time()

    def draw_time(self):
        minutes = "0" + str(self.duration//60) if self.duration//60 < 10 else self.duration//60
        seconds = "0" + str(self.duration%60) if self.duration%60 < 10 else self.duration%60
        pygame.draw.rect(screen, "white", (0, 0, 100, 45))
        screen.blit(helpers.draw_text(f"{minutes}:{seconds}", fonts.title_screen_font, "black"), (10, 0))
        pygame.display.update((0, 0, 100, 100))

    def start(self):

        bait = self.fishing_info.bait
        bait_func_button.image = bait.image
        draw_over_world(self)
        draw_panel(self)

        running = True
        self.see_panel = True
        self.see_fps = False


        self.draw_time()
        return self.over_world

    def inspect_players_every_fish(self, fisherman):

        screen.fill("burlywood3")


        screen.blit(helpers.draw_text(f"Tulokset: {self.current_lake_name.capitalize()}", fonts.big_font, "black"), (800, 70))
        screen.blit(helpers.draw_text(f"Yhteensä: {fisherman.fish_sum} g", fonts.title_screen_font, "black"), (800, 200))
        screen.blit(helpers.draw_text(f"Kalat: {fisherman.name}", fonts.title_screen_font, "black"), (800, 300))

        fish_text = [str(fish) + "\n" for fish in fisherman.single_fishes]

        box = ui_elements.Scrolling_box("darkgrey", "lightgrey", "black", 800, 400, 500, 500, fonts.title_screen_font, fish_text, 20)

        box.draw_text(screen)


        exit_image = pygame.image.load("graphics/fishing_window/back_button.png").convert()
        exit_button = button_classes.Button(50, 50, exit_image, 1)
        exit_button.draw(screen)


        pygame.display.update()

        watching_results = True

        while watching_results:
            pos = pygame.mouse.get_pos()
            left_click_pressed = pygame.mouse.get_pressed()[0]

            box.check_arrow_boxes_and_scroll_bar(screen, left_click_pressed, pos)

            if exit_button.check_logic():
                helpers.button_clicked(exit_button, screen)
                watching_results = False
                pygame.display.flip()
                return self.initialize_results
            

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None
                if event.type == pygame.MOUSEBUTTONDOWN:
                    box.check_mouse_scroll(screen, event.button)
                    pygame.display.flip()
                if event.type == pygame.KEYDOWN:
                    box.check_key_down(screen, event.key)


                    pygame.display.flip()
            clock.tick(30)


        return self.initialize_results

    
    def draw_results_screen(self):
        screen.fill("burlywood3")

        rects = []

        font_colors = ["yellow", "grey", "red", "black"]
        for i in range(1, len(self.all_players)+1):
            screen.blit(helpers.draw_text(f"{i}.", fonts.title_screen_font, font_colors[i-1]), (760, (i-1)*100+300))
            rects.append(pygame.Rect(1500, (i-1)*100+300, 50, 50))
            pygame.draw.rect(screen, "black", rects[i-1])
            pygame.draw.polygon(screen, "white", ((1515, (i-1)*100+315), (1515, (i-1)*100+335), (1540, (i-1)*100+325)))

        for i, player in enumerate(self.all_players):
            screen.blit(helpers.draw_text(f"{player.name}", fonts.title_screen_font, "black"), (800, i*100+300))
            screen.blit(helpers.draw_text(f"{player.fish_sum} g", fonts.title_screen_font, "black"), (1200, i*100+300))


        screen.blit(helpers.draw_text(f"Tulokset: {self.current_lake_name.capitalize()}", fonts.big_font, "black"), (800, 70))


        pygame.display.flip()

        return self.results_loop


    def results_loop(self):


        rects = []
        for i in range(1, len(self.all_players)+1):
            rects.append(pygame.Rect(1500, (i-1)*100+300, 50, 50))

        exit_image = pygame.image.load("graphics/fishing_window/back_button.png").convert()
        exit_button = button_classes.Button(50, 50, exit_image, 1)
        exit_button.draw(screen)
        pygame.display.flip()
        watching_results = True
        while watching_results:
            pos = pygame.mouse.get_pos()
            left_click_pressed = pygame.mouse.get_pressed()[0]


            if exit_button.check_logic():
                helpers.button_clicked(exit_button, screen)
                watching_results = False
                pygame.display.flip()

            

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for i, rect in enumerate(rects):
                        if rect.collidepoint(pos[0], pos[1]):
                            return functools.partial(self.inspect_players_every_fish, self.all_players[i])

            clock.tick(30)


        self.player_list.empty()
        self.bot_list.empty()
        music.set_song(pygame.mixer.music)

        return self.scene_after_game


        

class New_practice_game(Game):
    def __init__(self, duration, lake_name, season, scene_after_game):
        super().__init__(duration, lake_name, season, scene_after_game)

    


    def init(self, lake_name):

        # create lake related shit
        self.lake = Lake(lake_name)
        self.current_lake_name = lake_name

        if self.season == "spring":
            season_tiles = world_drawing.spring_over_world
        elif self.season == "autumn":
            season_tiles = world_drawing.autumn_over_world

        world_drawing.image_list_over_world = world_drawing.load_and_create_image(self.lake.TILE_SIZE, season_tiles)

        self.lake.map_surface = world_drawing.init_map_surf(self.lake)

        # scroll need to be initialized before initializing player
        self.scroll_x = initialize_scroll(self.lake.start_x, half_width, self.lake.world_size_x)
        self.scroll_y = initialize_scroll(self.lake.start_y, half_height, self.lake.world_size_y)


        # create player related things

        self.player = Player("graphics/animations/player_animation/frames/img")

        self.player.x_cor = determine_start_cor(self.lake.start_x, half_width, self.lake.world_size_x)
        self.player.y_cor = determine_start_cor(self.lake.start_y, half_height, self.lake.world_size_y)

        self.player.name = entire_program_state.player_name


        self.player_list.add(self.player)
        self.player.set_rect()
        self.player_list.update()
        self.player.init_self(self)
        self.player_list.draw(screen)


        # initialize self related things to this instance of a new game

        self.fishing_info = generate_fishing_info(self)
        self.lake.fish_schools = determine_initial_fish(self)
     
       
        self.borders_to_update = []
        self.panel_off_set = 200
        self.borders_to_update.append((0, 0, screen_width, screen_height-self.panel_off_set))
        self.see_panel = True


    def over_world(self):
        draw_over_world(self)
        pygame.display.flip()

        scene =  self.initialize_results
        running = True
        while self.duration >= 0 and running: 
            
            pressed_keys = pygame.key.get_pressed()

            if pressed_keys[pygame.K_UP]:
                self.scroll_y -= self.player.move_up(self)
                draw_over_world(self)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_LEFT]:
                self.scroll_x -= self.player.move_left(self)
                draw_over_world(self)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_DOWN]:            
                self.scroll_y += self.player.move_down(self)
                draw_over_world(self)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_RIGHT]:
                self.scroll_x += self.player.move_right(self)
                draw_over_world(self)
                pygame.display.update(self.borders_to_update)




            elif close_panel_button.check_logic():
                helpers.button_clicked(close_panel_button, screen)
                close_panel(self)

            elif self.see_panel:
                scene = check_panel_logic(self)
                if scene is not None:
                    if scene == "quit":
                        scene = self.initialize_results
                    running = False
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None

                if event.type == pygame.KEYDOWN:

                    if pygame.key.name(event.key) == "f8":
                        self.see_fps = not self.see_fps
                        if not self.see_fps:
                            screen.blit(self.lake.map_surface, (0, 0), (self.scroll_x, self.scroll_y, 1920, 1080))
                            self.player_list.draw(screen)
                    elif pygame.key.name(event.key) == "f6":
                        entire_program_state.fps = 60
                    elif pygame.key.name(event.key) == "f5":
                        entire_program_state.fps = 50
                    elif pygame.key.name(event.key) == "f4":
                        entire_program_state.fps = 40
                    elif pygame.key.name(event.key) == "f3":
                        entire_program_state.fps = 30


                if event.type == pygame.USEREVENT:
                    self.handle_time()

            if self.see_fps:
                screen.blit(helpers.draw_fps_text(screen, clock), (1600, 100))

                pygame.display.update(self.borders_to_update)

            clock.tick(entire_program_state.fps)
        
        if scene == self.initialize_results:
            records.update_records(self.player.fish, self)
        return scene

    def initialize_results(self):

        self.all_players = []
        self.all_players.append(self.player)

        for bot in self.bot_list:
            self.all_players.append(bot)

        for player in self.all_players:
            zum = 0
            player.single_fishes = []
            for fish in player.fish:
                player.single_fishes.append(fish)
                zum += fish.weight


            player.fish_sum = zum
            player.single_fishes = sorted(player.single_fishes, key=lambda fish: fish.weight, reverse=True)



        self.all_players = sorted(self.all_players, key=lambda p: p.fish_sum, reverse=True)

        return self.draw_results_screen




class New_tournament_game(Game):
    def __init__(self, duration, lake_name, season, scene_after_game):
        super().__init__(duration, lake_name, season, scene_after_game)


    def init(self, lake_name):

        # create lake related shit
        self.lake = Lake(lake_name)
        self.current_lake_name = lake_name

        seasons = {
                "spring": world_drawing.spring_over_world,
                "autumn": world_drawing.autumn_over_world
                }

        season_tiles = seasons[self.season]

        world_drawing.image_list_over_world = world_drawing.load_and_create_image(self.lake.TILE_SIZE, season_tiles)

        self.lake.map_surface = world_drawing.init_map_surf(self.lake)

        # scroll need to be initialized before initializing player
        self.scroll_x = initialize_scroll(self.lake.start_x, half_width, self.lake.world_size_x)
        self.scroll_y = initialize_scroll(self.lake.start_y, half_height, self.lake.world_size_y)


        # create player related things

        self.player = Player("graphics/animations/player_animation/frames/img")

        self.player.x_cor = determine_start_cor(self.lake.start_x, half_width, self.lake.world_size_x)
        self.player.y_cor = determine_start_cor(self.lake.start_y, half_height, self.lake.world_size_y)
        
        tour = load_tournament()
        self.player.name = tour.player_names[-1]


        self.player_list.add(self.player)
        self.player.set_rect()
        self.player_list.update()
        self.player.init_self(self)
        self.player_list.draw(screen)
        
        # initialize bot related things
        self.bot_list.empty()

        bot_names = ("Elastinen Elaska", "Later", "Köpi")
        for name in bot_names:
            bot = Bot(f"graphics/animations/elastinen_elaska/elastinen_elaska", name)
            bot.x_cor = determine_start_cor(self.lake.start_x, half_width, self.lake.world_size_x)
            bot.y_cor = determine_start_cor(self.lake.start_y, half_height, self.lake.world_size_y)

            bot.world_x = bot.x_cor + self.scroll_x
            bot.world_y = bot.y_cor + self.scroll_y

            self.bot_list.add(bot)
            bot.set_rect()
            bot.init_self(self)

        self.bot_list.update()
        self.bot_list.draw(screen)


        # initialize self related things to this instance of a new game

        self.fishing_info = generate_fishing_info(self)
        self.lake.fish_schools = determine_initial_fish(self)
     
       
        self.borders_to_update = []
        self.panel_off_set = 200
        self.borders_to_update.append((0, 0, screen_width, screen_height-self.panel_off_set))
        self.see_panel = True


    def start(self):

        bait = self.fishing_info.bait
        bait_func_button.image = bait.image
        draw_over_world(self)
        draw_panel(self)

        running = True
        self.see_panel = True
        self.see_fps = False

        for bot in self.bot_list:
            bot.cord_to_move_x = random.randint(0, self.lake.world_size_x)
            bot.cord_to_move_y = random.randint(0, self.lake.world_size_y)

            bot.moves_in_x = not (bot.cord_to_move_x == bot.world_x)
            bot.moves_in_y = not (bot.cord_to_move_y == bot.world_y)

            bot.moves = bot.moves_in_x or bot.moves_in_y

        self.draw_time()
        return self.over_world

    def handle_bots(self):
        for bot in self.bot_list:
            if bot.moves_in_x or bot.moves_in_y:
                bot.move((bot.cord_to_move_x, bot.cord_to_move_y))
                screen.blit(self.lake.map_surface, (bot.rect.x-10, bot.rect.y-10), (bot.world_x-10, bot.world_y-10, bot.world_x+70, bot.world_y+70))
                self.bot_list.draw(screen)
                self.player_list.draw(screen)
                if self.see_panel:
                    draw_panel(self)
                pygame.display.update((bot.rect.x-10, bot.rect.y-10, 75, 75))

    def over_world(self):
        draw_over_world(self)
        pygame.display.flip()
        
        scene = self.initialize_results

        running = True
        while self.duration >= 0 and running: 
            self.handle_bots()
           
            pressed_keys = pygame.key.get_pressed()

            if pressed_keys[pygame.K_UP]:
                change = self.player.move_up(self)
                self.scroll_y -= change
                for bot in self.bot_list:
                    bot.rect.y += change
                draw_over_world(self)
                self.bot_list.draw(screen)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_LEFT]:
                change = self.player.move_left(self)
                self.scroll_x -= change
                for bot in self.bot_list:
                    bot.rect.x += change
                draw_over_world(self)
                self.bot_list.draw(screen)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_DOWN]:            
                change = self.player.move_down(self)
                self.scroll_y += change
                for bot in self.bot_list:
                    bot.rect.y -= change
                draw_over_world(self)
                self.bot_list.draw(screen)
                pygame.display.update(self.borders_to_update)

            elif pressed_keys[pygame.K_RIGHT]:
                change = self.player.move_right(self)
                self.scroll_x += change
                for bot in self.bot_list:
                    bot.rect.x -= change
                draw_over_world(self)
                self.bot_list.draw(screen)
                pygame.display.update(self.borders_to_update)



            elif close_panel_button.check_logic():
                helpers.button_clicked(close_panel_button, screen)
                close_panel(self)

            elif self.see_panel:
                scene = check_panel_logic(self)
                if scene is not None:
                    if scene == "quit":
                        scene = self.initialize_results
                    running = False


            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    scene = None
                    running = False

                elif event.type == pygame.KEYDOWN:

                    if pygame.key.name(event.key) == "f8":
                        self.see_fps = not self.see_fps
                        if not self.see_fps:
                            screen.blit(self.lake.map_surface, (0, 0), (self.scroll_x, self.scroll_y, 1920, 1080))
                            self.player_list.draw(screen)

                    elif pygame.key.name(event.key) == "f6":
                        entire_program_state.fps = 60
                    elif pygame.key.name(event.key) == "f5":
                        entire_program_state.fps = 50
                    elif pygame.key.name(event.key) == "f4":
                        entire_program_state.fps = 40
                    elif pygame.key.name(event.key) == "f3":
                        entire_program_state.fps = 30

                elif event.type == pygame.USEREVENT:
                    self.handle_time()

                elif event.type == BOT_MOVES_EVENT:

                    for bot in self.bot_list:
                        if not (bot.moves_in_x or bot.moves_in_y):
                            bot.moves_in_x = True
                            bot.moves_in_y = True
                            bot.cord_to_move_x = random.randint(1, self.lake.world_size_x)
                            bot.cord_to_move_y = random.randint(1, self.lake.world_size_y)

                elif event.type == UPDATE_BOTS_EVENT:
                    for bot in self.bot_list:
                        bot.set_new_values(self)
                        if bot.world_x > bot.cord_to_move_x and bot.moves_in_x and bot.tile_on_left not in tiles_can_go_through:
                            bot.moves_in_x = False
                            bot.cord_to_move_x = random.randint(bot.world_x, max(bot.world_x + 300, self.lake.world_size_x))

                        if bot.world_x < bot.cord_to_move_x and bot.moves_in_x and bot.tile_on_right not in tiles_can_go_through:
                            bot.moves_in_x = False
                            bot.cord_to_move_x = random.randint(max(0, bot.world_x - 300), bot.world_x)

                        if bot.world_y > bot.cord_to_move_y and bot.moves_in_y and bot.tile_above not in tiles_can_go_through:
                            bot.moves_in_y = False
                            bot.cord_to_move_y = random.randint(bot.world_y, max(bot.world_y + 300, self.lake.world_size_y))

                        if bot.world_y < bot.cord_to_move_y and bot.moves_in_y and bot.tile_below not in tiles_can_go_through:
                            bot.moves_in_y = False
                            bot.cord_to_move_y = random.randint(max(0, bot.world_y - 300), bot.world_y)



            if self.see_fps:
                screen.blit(helpers.draw_fps_text(screen, clock), (1600, 100))
                pygame.display.update(self.borders_to_update)

            clock.tick(entire_program_state.fps)
        

        if scene == self.initialize_results:
            tour = load_tournament()
            scored_players = self.determine_players_who_scored()
            tour.update_score(scored_players)
            records.update_records(self.player.fish, self)

            most_fish = len(self.player.fish) > 0

            for bot in self.bot_list:
                if sum(map(lambda x: x.weight, bot.fish)) > sum(map(lambda x: x.weight, self.player.fish)):
                    most_fish = False
            if most_fish:
                pygame.mixer.music.stop()
                pygame.mixer.music.load("music/short_clips/voitto.wav")
                pygame.mixer.music.play(1)

        return scene

    def determine_players_who_scored(self):
        self.all_players = []
        self.all_players.append(self.player)

        for bot in self.bot_list:
            self.all_players.append(bot)

        for player in self.all_players:
            zum = 0
            player.single_fishes = []
            for fish in player.fish:
                player.single_fishes.append(fish)
                zum += fish.weight


            player.fish_sum = zum
            player.single_fishes = sorted(player.single_fishes, key=lambda fish: fish.weight, reverse=True)


        players_who_scored = []
        self.all_players = sorted(self.all_players, key=lambda p: p.fish_sum, reverse=True)
        for player in self.all_players:
            if player.fish_sum > 0:
                players_who_scored.append(player)

        return players_who_scored

    def initialize_results(self):
        return self.draw_results_screen

class Tournament:
    def __init__(self, maps):
        self.maps = maps
        self.seasons = ("spring", "spring", "autumn", "autumn")
        self.tournament_game_id = 0

        self.player_names = ("Elastinen Elaska", "Köpi", "Later", entire_program_state.player_name)

        self.player_points = {
                "Elastinen Elaska": 0,
                "Köpi": 0,
                "Later": 0,
                self.player_names[-1]: 0
                }


    def update_score(self, players):
        scores = (10, 8, 7, 0)
        
        for x in range(len(players)): # players list (argument) is a sorted list, so scores are correct from looping order
            self.player_points[players[x].name] += scores[x]
        

        self.player_points = {k: v for k, v in sorted(self.player_points.items(), key=lambda item: item[1], reverse=True)}

        save_tournament(self)




class Bait:
    def __init__(self, name, image):
        self.name = name
        self.image = image

 

# this class handles global things related to whole program
class Entire_program_state:
    def __init__(self):
        self.player_name = "" # in the very beginning players name is just an underscore
        self.fps = 60



## Constants that WILL NOT change during execution of different maps


# game related constants

speed = 5

DISTANCE_FROM_BORDER = 940



# translates english into Finnish
seasons_dict = {
        "spring": "kevät",
        "autumn": "syksy"
        }


half_width = screen_width//2
half_height = screen_height//2


fishing_bar_x = 100
fishing_bar_y = 250
fishing_bar_width = 60
fishing_bar_height = 500

# Group constants for sprites

bobber_list = pygame.sprite.Group()

bobber = sprite_classes.Animated_sprite(660, 220, "graphics/fishing_window/onkikoho", 6, 5)
bobber.set_rect()
bobber_list.add(bobber)
bobber.update()

# tiles game.player can row through

tiles_can_go_through = (0, 2, 11)

# button and image constants

practice_surface = pygame.image.load("graphics/buttonimages/harjoittele.png").convert_alpha()
settings_surface = pygame.image.load("graphics/buttonimages/settings_button.png").convert_alpha()
ulos_surface = pygame.image.load("graphics/buttonimages/ulos_nappi.png").convert_alpha()
tournament_surface = pygame.image.load("graphics/buttonimages/tournament.png").convert_alpha()
records_surface = pygame.image.load("graphics/buttonimages/records.png").convert_alpha()

depth_chart_img = pygame.image.load("graphics/buttonimages/depth_chart.png").convert_alpha()

# loading screen image


start_fishing_surface = pygame.image.load("graphics/buttonimages/start_fishing_button.png").convert_alpha()

bait_surface = pygame.image.load("graphics/buttonimages/bait_button.png").convert_alpha()
fish_bait_surface = pygame.image.load("graphics/buttonimages/fish_bait_image.png").convert()

use_rod_surface = pygame.image.load("graphics/fishing_window/fishing_rod.png").convert_alpha()
back_to_over_world_surface = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()


bait_func_button = button_classes.Button_with_function(1100, DISTANCE_FROM_BORDER, bait_surface, 1, "bait_func")
start_fishing_func_button = button_classes.Button_with_function(900, DISTANCE_FROM_BORDER, start_fishing_surface, 1, "open_fishing_window")

choose_bait_button = button_classes.Bait_box_button(850, 550, bait_surface, 1, 0)
choose_fish_bait_button = button_classes.Bait_box_button(1050, 550, fish_bait_surface, 1, 1)

use_rod_button = button_classes.Button_with_function(900, DISTANCE_FROM_BORDER, use_rod_surface, 1, "rod_down")
back_to_over_world_button = button_classes.Button_with_function(500, DISTANCE_FROM_BORDER, back_to_over_world_surface, 1, "back_to_over_world")

exit_panel_img = pygame.image.load("graphics/buttonimages/close_panel.png").convert()
close_panel_button = button_classes.Button_with_function(50, DISTANCE_FROM_BORDER, exit_panel_img, 1, "close_panel")


quit_playing_img = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()
quit_playing_button = button_classes.Button_with_function(1780, DISTANCE_FROM_BORDER, quit_playing_img, 1, "main")

depth_chart_button = button_classes.Button_with_function(300, DISTANCE_FROM_BORDER, depth_chart_img, 1, "show_depth_chart")

panel_settings_button = button_classes.Button_with_function(1600, DISTANCE_FROM_BORDER, settings_surface, 1, "settings")


ground_chart_img = pygame.image.load("graphics/buttonimages/ground_chart.png").convert_alpha()
ground_chart_button = button_classes.Button_with_function(550, DISTANCE_FROM_BORDER, ground_chart_img, 1, "show_ground_chart")


bait_button_list = (choose_bait_button, choose_fish_bait_button)

panel_button_list = (start_fishing_func_button, bait_func_button, quit_playing_button, panel_settings_button, depth_chart_button, ground_chart_button)
fishing_window_gui_button_list = (use_rod_button, back_to_over_world_button, bait_func_button)



#practice_text = button_classes.Button(500, 500, practice_surface, 1)

practice_text = ui_elements.Clickable_text(500, 400, 300, 50, 0, "Harjoittelu", fonts.medium_font)

settings_button = button_classes.Button(30, 50, settings_surface, 1)

ulos_text = ui_elements.Clickable_text(500, 700, 300, 100, 0, "Pois, ja äkkiä!", fonts.medium_font)
tournament_text = ui_elements.Clickable_text(500, 500, 300, 60, 0, "Turnaus", fonts.medium_font)
records_text = ui_elements.Clickable_text(500, 600, 300, 60, 0, "Ennätykset", fonts.medium_font)

back_img = pygame.image.load("graphics/fishing_window/back_button.png").convert_alpha()
back_button = button_classes.Button(50, 935, back_img, 1)

gui_buttons = (back_button, settings_button)

main_screen_buttons = [settings_button]
main_screen_texts = (practice_text, ulos_text, tournament_text, records_text)


left_fishing_window = pygame.image.load("graphics/fishing_window/left_panel.png").convert()


continue_text = ui_elements.Clickable_text(800, 400, 250, 50, 0, "Jatka turnausta", fonts.title_screen_font)
new_tournament_text = ui_elements.Clickable_text(800, 500, 200, 50, 0, "Uusi turnaus", fonts.title_screen_font)
texts = (continue_text, new_tournament_text)

# Initialize different baits


worm_bait = Bait("worm_bait", bait_surface)
fish_bait = Bait("fish_bait", fish_bait_surface)
bait_list = (worm_bait, fish_bait)


def update_animation(sprite):
    sprite.frame += 1
    if sprite.frame == len(sprite.images):
        sprite.frame = 0
    sprite.image = sprite.images[sprite.frame]


def draw_buttons(button_list):
    for button in button_list:
        button.draw(screen)
    pygame.display.update((0, 1920-200, 1920, 200))


def draw_bait_box():
    pygame.draw.rect(screen, "darkblue", (800, 500, 400, 200))
    draw_buttons(bait_button_list)



def bait_func(game, next_scene):
    running = True

    draw_bait_box()
    bait = game.fishing_info.bait
    while running and game.duration >= 0:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            if event.type == pygame.USEREVENT:
                game.handle_time()

        for button in bait_button_list:
            if button.check_logic():
                bait = bait_list[button.bait_id]
                button.draw_clicked_state(screen)
                pygame.display.flip()
                helpers.avoid_second_click()
                game.fishing_info.bait = bait

                running = False

        pygame.display.update(((800, 500, 400, 200), (0, 0, 100, 100)))
        clock.tick(30)

    bait_func_button.image = bait.image

    return next_scene


def draw_got_away(game):
    initialize_fishing_window(game)
    screen.blit(helpers.draw_text("Kala irtosi!", fonts.title_screen_font, "black"), (700, 150))
    pygame.display.flip()
    pygame.time.wait(1000)
    return functools.partial(open_fishing_window, game)


def get_moving_speed(weigth):
    # f(x) = 0,000150125104253*x + 0,1984987 / suora pisteiden (10; 0,2), (12000;2) kautta
    return 0.000150125*weigth + 0.1984987

def get_direction_change_time(weight):
    # f(x) = -185/2398*x+1199925/1199, return an integer in MILLISECONDS
    return int(-(185/2398)*weight+1199925/1199)

def raising_animation(game, fish_weight, fish_arg):


    next_scene = functools.partial(display_raised_fish, fish_arg, fish_arg.weight, game)

    start_x = half_width
    pygame.draw.line(screen, "grey", (half_width, 0), (start_x, screen_height-300), width=4)
    pygame.display.flip()

    change = 0
    change_constant = get_moving_speed(fish_arg.weight)
    direction = random.choice(("left", "right"))
    pygame.time.set_timer(FISH_MOVES_EVENT, 10)

    gets_away = fish_arg.gets_away()
    get_away_height = random.randint(2, game.fishing_bar.center[1]-game.fishing_bar.y) 

    rising_speed = fish_weight
    constant = game.lake.underwater[game.player.current_row][game.player.current_column] 
    if constant == 0:
        constant = 1
    rising_speed = constant*rising_speed

    pygame.time.set_timer(CHANGE_DIRECTION_EVENT, get_direction_change_time(fish_arg.weight))

    running = True
    while game.fishing_bar.center[1] > game.fishing_bar.top_border and running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                running = False
            if event.type == pygame.USEREVENT:
                game.handle_time()
            
            if event.type == CHANGE_DIRECTION_EVENT:
                if start_x+change_constant*change < 550:
                    direction = "left"
                elif start_x+change_constant*change > 1200:
                    direction = "right"
                else:
                    direction = random.choice(("left", "right"))
                
        

            if event.type == FISH_MOVES_EVENT:

                if direction == "left":
                    change += 5
                elif direction == "right":
                    change -= 5

                screen.blit(left_fishing_window, (700, 0))
                screen.blit(left_fishing_window, (200, 0))
                pygame.draw.line(screen, "grey", (half_width, 0), (start_x+change_constant*change, screen_height-300), width=4)
                pygame.display.update((200, 0, 1200, 800))

        game.fishing_bar.move(-rising_speed)
        game.fishing_bar.draw(screen)
        if gets_away and game.fishing_bar.center[1] < game.fishing_bar.y + get_away_height:
            next_scene = functools.partial(draw_got_away, game)
            running = False

        clock.tick(30)
        pygame.display.update((0, 0, 200, 900))

    
    # after fish is on dry land
    game.fishing_bar.set_active()
    if not (gets_away and game.fishing_bar.center[1] < game.fishing_bar.y + get_away_height):
        game.player.fish.append(fish_arg)

    game.lake.fish_schools = delete_raised_fish(fish_arg, game)
    return next_scene


def start_raising_fish(fish_arg, game):
    screen.blit(left_fishing_window, (700, 0))

    weight = fish_arg.weight 
    fish_weight = determine_raise_speed(fish_arg)
    draw_buttons(fishing_window_gui_button_list)

    return functools.partial(raising_animation, game, fish_weight, fish_arg)

def display_raised_fish(fish_arg, weight, game):

    # animation that shows fish raisin from water

    # define picture to correct canvas etc.
    picture = pygame.image.load(fish_arg.image).convert_alpha()

    scale = fish_arg.weight * 0.005


    picture = pygame.transform.rotozoom(picture, 0, scale)

    img_width = picture.get_width()
    img_height = picture.get_height()
    x_cor = half_width-img_width//2 

    next_scene = functools.partial(open_fishing_window, game)

    # now show fish raising from the depths

    x = 0
    running = True
    while running:

        screen.blit(left_fishing_window, (700, 0))
        screen.blit(left_fishing_window, (0, 0))
        screen.blit(left_fishing_window, (1400, 0))
        draw_buttons(fishing_window_gui_button_list)
        screen.blit(picture, (x_cor, 900-x*100))
        pygame.display.update((0, 0, 1920, 800))
        game.draw_time()
        pygame.time.wait(100)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                running = False

            if event.type == pygame.USEREVENT:
                game.handle_time()


        if 900-x*100+img_height < 0:
            running = False

        x += 1
        clock.tick(30)



    # After fish rised from the depths this happens:

    picture = pygame.transform.rotate(picture, -90)
                
                        # needst to be height in x coordinate because image is flipped 90 degree
    screen.blit(picture, (half_width-img_height//2, half_height-img_width//2))


    screen.blit(helpers.draw_text(f"{str(fish_arg)}", fonts.title_screen_font, "black"), (600, 100))
    game.draw_time()

    pygame.display.flip()
    start = time.time()
    while time.time()-3 < start:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            if event.type == pygame.USEREVENT:
                game.handle_time()
        clock.tick(30)

    picture = None #### setting a picture to type "None" dereferences it and avoids memory leak
    return next_scene

def determine_raise_speed(fish):
    # f(x) = e^(-0,002x)+0,015
    return 2.718281828**(-0.002*fish.weight)+0.015

def play_fake_waiting_animation(game):
    player_waits = True
    game.draw_time()
    while player_waits and game.duration >= 0:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            if event.type == pygame.USEREVENT:
                game.handle_time()
            if event.type == BOBBER_UP_AND_DOWN_EVENT:
                update_bobber()
            if event.type == MOVE_BOBBER_EVENT:
                move_bobber()
                
        for button in fishing_window_gui_button_list:
            if button.check_logic():
                button.draw_clicked_state(screen)
                
                if button.return_function() == "rod_down":
                    player_waits = False

                    game.fishing_bar.set_active()
        clock.tick(30)
                    
    return functools.partial(open_fishing_window, game)

def move_bobber():
    if bobber.rect.y == 220:
        bobber.rect.y = 225
    elif bobber.rect.y == 225:
        bobber.rect.y = 220

def update_bobber():
    update_animation(bobber)
    
    screen.blit(left_fishing_window, (700, 0))
    draw_buttons(fishing_window_gui_button_list)
    bobber.update()
    bobber_list.draw(screen)
    pygame.display.flip()
    pygame.display.update((700, 0, 700, 560))


def play_waiting_animation(time_to_wait, game, fish):

    player_waits = True
    start_time = time.time()
    game.draw_time()
    while time.time() - start_time < time_to_wait and player_waits and game.duration >= 0: # checks when either game.player doesn't wait anymore or fish bites

        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:
                game.handle_time()
            if event.type == pygame.QUIT:
                return None
            if event.type == BOBBER_UP_AND_DOWN_EVENT:
                update_bobber()
            if event.type == MOVE_BOBBER_EVENT:
                move_bobber()
                
        for button in fishing_window_gui_button_list:
            if button.check_logic():
                helpers.button_clicked(button, screen)
                if button.return_function() == "rod_down":
                    player_waits = False


        pygame.display.update(((700, 240, 500, 500), (fishing_bar_x, fishing_bar_y, fishing_bar_width, fishing_bar_height)))
        pygame.time.wait(10)
        clock.tick(30)


    if player_waits:
        return functools.partial(start_raising_fish, fish, game)
    
    return functools.partial(open_fishing_window, game)

def remove_empty_schools(game):
    return [school for school in game.lake.fish_schools if school.members]

def get_fish_schools_player_collides_with(game):
    return [school for school in game.lake.fish_schools if matplotlib.path.Path(school.coordinates).contains_point((game.fishing_info.x_cor, game.fishing_info.y_cor))]

def get_biting_fish_schools(fish_schools_arg, game):
    temp_fish_schools = []
    for school in fish_schools_arg:
        flag = False
        for fish in school.members:
            if fish.set_biting_time(game.fishing_info) > 0: 
                flag = True
        if flag:
            temp_fish_schools.append(school)

    return temp_fish_schools

def get_biting_fish_in_school(fish_school_arg, game):
    return [fish for fish in fish_school_arg.members if fish.set_biting_time(game.fishing_info) > 0]

def delete_raised_fish(fish_arg, game):
    temp = []

    for school in game.lake.fish_schools:
        temp_fish_list = []
        for fish in school.members:
            if fish is not fish_arg:
                temp_fish_list.append(fish)
        temp.append(fish_info_classes.Fish_school(school.coordinates, temp_fish_list))

    return temp



def draw_rod_down(game):

    initialize_fishing_window(game)

    game.fishing_bar.set_active()
    game.fishing_bar.draw(screen)

    bobber_list.draw(screen)
    game.draw_time()
    pygame.display.update(((700, 240, 500, 500), (fishing_bar_x, fishing_bar_y, fishing_bar_width, fishing_bar_height)))

def determine_eating_fish(game):
    # if there are empty schools, it removes those 
    game.lake.fish_schools = remove_empty_schools(game)
    # get every fish school that game.player is inside of
    
    fish_schools = get_fish_schools_player_collides_with(game)

    return functools.partial(try_to_get_fish, game, fish_schools)


def try_to_get_fish(game, fish_schools):

    if not fish_schools:
        return functools.partial(play_fake_waiting_animation, game)
    

    else:   # game.player is inside fish school, so check biting
        temp_fish_schools = get_biting_fish_schools(fish_schools, game) # get fish schools with fishes biting in them

        a_fish_bites = False
        for school in temp_fish_schools:
            for fish in get_biting_fish_in_school(school, game):
                fish.check_weight(game.fishing_info)

                if fish.biting_time > 0: 
                    a_fish_bites = True
                    scene = play_waiting_animation(fish.biting_time, game, fish)
                    return scene


        if not a_fish_bites:
            return functools.partial(play_fake_waiting_animation, game)


    initialize_fishing_window(game)
                    

def initialize_fishing_window(game):

    depth = game.lake.underwater[game.player.current_row][game.player.current_column] 
    if depth == 0:
        depth = 1
    game.fishing_bar = ui_elements.Fishing_bar((0, 4, 214), fishing_bar_x, fishing_bar_y, fishing_bar_width, fishing_bar_height, (181, 181, 181), "blue", depth/(len(world_drawing.picture_list_underwater)-1))

    screen.blit(left_fishing_window, (0, 0))
    screen.blit(left_fishing_window, (700, 0))
    screen.blit(left_fishing_window, (1400, 0))

    game.fishing_bar.set_depth(depth)
    game.fishing_bar.draw(screen)


    draw_buttons(fishing_window_gui_button_list)

    game.draw_time()

    pygame.display.flip()


def open_fishing_window(game):

    game.fishing_info.x_cor = game.player.rect.x+game.scroll_x
    game.fishing_info.y_cor = game.player.rect.y+game.scroll_y 

    initialize_fishing_window(game)
    in_fishing_window = True
    game.draw_time()
    while in_fishing_window and game.duration >= 0:
        game.fishing_bar.draw(screen)

        for button in fishing_window_gui_button_list:
            if button.check_logic():
                helpers.button_clicked(button, screen)
                button_func = button.return_function()
                if button_func == "back_to_over_world":
                    return game.over_world

                if button_func == "rod_down":
                    draw_rod_down(game)
                    return functools.partial(determine_eating_fish, game)

                if button.return_function() == "bait_func":
                    return functools.partial(bait_func, game, functools.partial(open_fishing_window, game))

        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:
                game.handle_time()
            if event.type == pygame.QUIT:
                return None
                
        pygame.display.update((fishing_bar_x, fishing_bar_y, fishing_bar_width, fishing_bar_height))
        
        clock.tick(30)

    game.fishing_bar = None
    return game.over_world



def close_panel(game):
    game.see_panel = not game.see_panel
    if not game.see_panel:
        game.panel_off_set = 0
        game.borders_to_update[0] = (0, 0, screen_width, screen_height-game.panel_off_set)
        draw_over_world(game)
    else:
        game.panel_off_set = 200
        game.borders_to_update[0] = (0, 0, screen_width, screen_height-game.panel_off_set)
        draw_over_world(game)
        draw_panel(game)
    pygame.display.flip()

def draw_over_world(game):

    screen.blit(game.lake.map_surface, (0, 0), (game.scroll_x, game.scroll_y, 1920, 1080))

    close_panel_button.draw(screen)
    game.player_list.update()    
    game.player_list.draw(screen)

    game.draw_time()


    if game.see_panel:
        draw_panel(game)

    return game.over_world


def draw_panel(game):
    pygame.draw.rect(screen, "lightblue", (0, 1080-200, 1920, 200))
    pygame.draw.rect(screen, "black", (0, 1080-200, 1920, 8))
    draw_buttons(panel_button_list)
    close_panel_button.draw(screen) 
    screen.blit(game.fishing_info.sky_img, (1400, 950))


    pygame.display.update((0, 1080-200, 1920, 200))


def open_ground_chart(game):
    percentage_x = (game.player.rect.x + game.scroll_x) / game.lake.world_size_x
    correct_cor_x = game.lake.ground_chart.get_width() * percentage_x + half_width-game.lake.ground_chart.get_width()//2

    percentage_y = (game.player.rect.y + game.scroll_y) / game.lake.world_size_y
    correct_cor_y = game.lake.ground_chart.get_height() * percentage_y + half_height -game.lake.ground_chart.get_height()//2 - 95

    screen.blit(game.lake.ground_chart, (half_width-game.lake.ground_chart.get_width()//2, 150))

    pygame.draw.circle(screen, "red", (correct_cor_x, correct_cor_y), 5)
    
    pygame.display.flip()
    watching = True
    while watching:
        if depth_chart_button.check_logic():
            return functools.partial(open_depth_chart, game)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            elif event.type == pygame.MOUSEBUTTONDOWN:
                watching = False
            elif event.type == pygame.USEREVENT:
                game.handle_time()
        
        clock.tick(20)


    return game.over_world

def open_depth_chart(game):
    
    screen.blit(game.lake.depth_chart, (half_width-game.lake.depth_chart.get_width()//2, 150))
    
    pygame.display.flip()
    watching = True
    while watching:
        if ground_chart_button.check_logic():
            return functools.partial(open_ground_chart, game)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return None
            elif event.type == pygame.MOUSEBUTTONDOWN:
                watching = False
            elif event.type == pygame.USEREVENT:
                game.handle_time()
        
        clock.tick(20)


    return game.over_world

def check_panel_logic(game):
    next_scene = None
    for button in panel_button_list:
        if button.check_logic():
            draw_over_world(game)
            draw_panel(game)
            helpers.button_clicked(button, screen)

            func_to_call = button.return_function()
            if func_to_call == "bait_func":
                next_scene = functools.partial(bait_func, game, functools.partial(draw_over_world, game))
            elif func_to_call == "open_fishing_window":
                next_scene = functools.partial(open_fishing_window, game)
            elif func_to_call == "main":
                next_scene = "quit"
            elif func_to_call == "settings":
                next_scene = functools.partial(settings_menu.begin(screen, clock, game.over_world))
            elif func_to_call == "show_depth_chart":
                next_scene = functools.partial(open_depth_chart, game)
            elif func_to_call == "show_ground_chart":
                next_scene = functools.partial(open_ground_chart, game)

            break 
    return next_scene

def determine_start_cor(start, half_dist, size):
    if start < half_dist:
        return start
    elif start > size-half_dist:
        return 2*half_dist-(size-start)
    else:
        return half_dist

def initialize_scroll(start, half_dist, size):
    if start < half_dist:
        return 0
    elif start > size-half_dist:
        return size - 2*half_dist
    else:
        return start - half_dist




def generate_weather():
    sky_conditions = ("sunny", "cloudy")
    sky = random.choice(sky_conditions)
    return sky


def generate_fishing_info(game):
    return fish_info_classes.Fishing_info(game.player.rect.x+game.scroll_x, game.player.rect.y+game.scroll_y, worm_bait, generate_weather())


def determine_initial_fish(game):

    all_fish_schools = data_handling.load_pickled_fish(game.lake.name, game.season)

    temp = []

    # checks here which fishes start eating at the very beginning of each game

    for fish_school in all_fish_schools:
        temp_fish_school = fish_info_classes.Fish_school(fish_school.coordinates, [])

        for fish in fish_school.members:
            fish.set_weight()
            if fish.test_eating(game.fishing_info):
                temp_fish_school.members.append(fish)
                for bot in game.bot_list:
                    if bot.name == "Later":
                       if isinstance(fish, fish_info_classes.Perch) and random.choice((True, False, False, False)):
                           bot.fish.append(fish_info_classes.Pseudo_fish(random.randint(fish.min_size, fish.max_size), "Ahven"))
                       elif isinstance(fish, fish_info_classes.Bream) and random.randint(1, 10) == 2:
                           bot.fish.append(fish_info_classes.Pseudo_fish(random.randint(fish.min_size, fish.max_size), "Lahna"))

                    elif bot.name == "Elastinen Elaska":
                        if random.choice((False, True, False, False, False, False)):
                            bot.fish.append(fish_info_classes.Pseudo_fish(random.randint(fish.min_size, fish.max_size), fish.name))

                    elif bot.name == "Köpi":
                        if random.choice((False, False, False, True, False, False, False, False, False, False, False)):
                            bot.fish.append(fish_info_classes.Pseudo_fish(random.randint(fish.min_size, fish.max_size), fish.name))


        if temp_fish_school.members: ### checks if any fish are eating in this school using "pythonic" garbage syntax
            temp.append(temp_fish_school)

    # now all fishes that initially don't eat are removed


    for fish_school in temp:
        fish_school.members = randomize_fish(fish_school.members, len(fish_school.members)) 


    return temp

def randomize_fish(fish_school, length):
    return random.sample(fish_school, k=length)



##///////////////////////////////////////////////////////////////////////
##### All tournament related stuff

def save_tournament(tour):
    with open("tournament/tournament.pickle", "wb") as picklefile:
        pickle.dump(tour, picklefile)

def load_tournament():
    with open("tournament/tournament.pickle", "rb") as picklefile:
        tourn = pickle.load(picklefile)
    return tourn



def continue_tournament():


    tour = load_tournament()
    
    txt = []
    for i in range(len(tour.maps)):
        txt.append(tour.maps[i].capitalize() + "  --  " + seasons_dict[tour.seasons[i]] + "\n")


    start_text = ui_elements.Clickable_text(550, 700, 500, 140, 0, "Aloita peli!", fonts.start_tournament_font)
    games_box = ui_elements.Scrolling_box("darkgrey", "lightgrey", "black", 550, 270, 700, 300, fonts.title_screen_font, txt, 10)

    screen.fill("lightblue")


    back_button.draw(screen)

    start_text.draw(screen)
    games_box.draw_text(screen)

    screen.blit(helpers.draw_text("Jatka turnausta:", fonts.big_font, "black"), (550, 80))
    pygame.draw.rect(screen, "red", (550+700-80, 7+280+tour.tournament_game_id*6*10+8, 30, 30))

    for i in range(1, len(tour.maps)+1):
        pygame.draw.line(screen, "black", (560, 280+i*60), (550+700-80, 280+i*60), width=3)


    screen.blit(helpers.draw_text("Pisteet:", fonts.big_font, "black"), (1300, 80))
    sorted_players = []


    for i, name in enumerate(tour.player_points):
        screen.blit(helpers.draw_text(f"{name}", fonts.title_screen_font, "black"), (1300, 300+i*100))
        screen.blit(helpers.draw_text(f"{tour.player_points[name]}", fonts.title_screen_font, "black"), (1700, 300+i*100))


    pygame.display.flip()

    next_scene = None

    starting = True
    while starting:

        pressed = pygame.mouse.get_pressed()[0]
        pos = pygame.mouse.get_pos()


        if back_button.check_logic():
            helpers.button_clicked(back_button, screen)
            next_scene = tournament_screen
            starting = False
         
        elif pressed and start_text.rect.collidepoint(pos[0], pos[1]) and tour.tournament_game_id < len(tour.seasons):
            helpers.text_clicked(start_text, screen)
            game = New_tournament_game(60*5, tour.maps[tour.tournament_game_id], tour.seasons[tour.tournament_game_id], continue_tournament)
            tour.tournament_game_id += 1
            save_tournament(tour)
            next_scene = game.start
            starting = False


        games_box.check_arrow_boxes_and_scroll_bar(screen, pressed, pos)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                starting = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                games_box.check_mouse_scroll(screen, event.button)
        
                pygame.display.update((550, 270, 700, 300))

        clock.tick(30)

    return next_scene


def randomize_maps():
    maps = ["piikkiönlahti", "sattajärvi", "kivijärvi", "kolkka"]
    
    lake_list = []
    for x in range(4): # lenght of tournament is 3 maps, might change in the future when more maps are made
        lake = random.randint(0, len(maps)-1) 
        lake_list.append(maps.pop(lake))

    return lake_list


def create_new_tournament():

    maps = randomize_maps()
    tournament = Tournament(maps)

    save_tournament(tournament)


    return tournament_screen



def init_tournament_screen():

    new_tournament_text.font_color = new_tournament_text.non_active_font_color
    continue_text.font_color = continue_text.non_active_font_color
    screen.fill("lightblue")

    screen.blit(helpers.draw_text("Turnaus", fonts.big_font, "black"), (800, 100))

    for button in gui_buttons:
        button.draw(screen)

    for text in texts:
        text.draw(screen)


    pygame.display.flip()

def tournament_screen():

    next_scene = main

    init_tournament_screen() 
    in_tournament_screen = True

    
    while in_tournament_screen:

        pos = pygame.mouse.get_pos()
        pressed = pygame.mouse.get_pressed()[0]

        if back_button.check_logic():
            helpers.button_clicked(back_button, screen)
            in_tournament_screen = False

        elif settings_button.check_logic():
            helpers.button_clicked(settings_button, screen)
            next_scene = functools.partial(settings_menu.begin, screen, clock, tournament_screen)
            in_tournament_screen = False

        elif new_tournament_text.rect.collidepoint(pos[0], pos[1]) and pressed:
            helpers.text_clicked(new_tournament_text, screen)
            next_scene = create_new_tournament
            in_tournament_screen = False

        elif continue_text.rect.collidepoint(pos[0], pos[1]) and pressed:
            helpers.text_clicked(continue_text, screen)
            next_scene = continue_tournament 
            in_tournament_screen = False
            


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                next_scene = None
                in_tournament_screen = False

        clock.tick(30)

    return next_scene





def practice_screen():
    lake_name, season = loading_and_title_screen.practice_screen(screen, clock, pygame.mixer.music) # return a tuple to those two variables

    # if player wanted to exit screen, then name is None and return back to main scene (into scene variable)
    # else game starts and main is returned only after playing the game

    if lake_name is None:
        return main

    game = New_practice_game(60*60, lake_name, season, main)
    return game.start    
    



def init_choosing_game_mode(splash, box):
    for text in main_screen_texts:
        text.font_color = text.non_active_font_color
    screen.fill("lightblue")

    screen.blit(helpers.draw_text(splash, fonts.title_screen_font, "black"), (500, 950))
    screen.blit(helpers.draw_text("Nimi: ", fonts.title_screen_font, "black"), (500, 100))

    box.draw(screen, add_to_end="_")

    draw_buttons(main_screen_buttons)
    draw_buttons(main_screen_texts)

    pygame.display.flip()
    
    

def get_splash_text():
    with open("miscellaneous/splash_texts.txt") as txtfile:
        data = txtfile.readlines()
    data = [x.strip() for x in data]
    splash_text = random.choice(data)
    return splash_text



def main():
    MAX_WIDTH = 700

    name_box = ui_elements.Typing_box(500, 190, 900, 120, "Kirjoita nimesi:")
    if entire_program_state.player_name != "_":
        name_box.written_text = entire_program_state.player_name
    name_box.font = pygame.font.Font("SHPinscher-Regular.otf", 80)

    
    init_choosing_game_mode(splash_text, name_box)

    under_score = True
    
    choosing_game_mode = True 
    next_scene = main
    while choosing_game_mode:

        if practice_text.check_logic():
            helpers.text_clicked(practice_text, screen)
            next_scene = practice_screen
            choosing_game_mode = False

        elif tournament_text.check_logic():
            helpers.text_clicked(tournament_text, screen)
            next_scene = functools.partial(tournament_screen)
            choosing_game_mode = False

        elif settings_button.check_logic():
            helpers.button_clicked(settings_button, screen)
            next_scene = functools.partial(settings_menu.begin, screen, clock, main)
            choosing_game_mode = False

        elif records_text.check_logic():
            helpers.text_clicked(records_text, screen)
            pygame.mixer.music.stop()
            pygame.mixer.music.load("music/short_clips/voitto.wav")
            pygame.mixer.music.play(-1)
            next_scene = functools.partial(records.show_records, main, screen)
            choosing_game_mode = False

        elif ulos_text.check_logic():
            helpers.text_clicked(ulos_text, screen)
            next_scene = None
            choosing_game_mode = False


        for event in pygame.event.get():
            if event.type == FLASH_UNDERSCORE_EVENT:
                under_score = not under_score
                if under_score:
                    name_box.draw(screen, add_to_end="_")
                else:
                    name_box.draw(screen, add_to_end="")

                pygame.display.flip()

            if event.type == pygame.QUIT:
                next_scene = None
                choosing_game_mode = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    name_box.written_text = name_box.written_text[:-1]
                elif event.key != pygame.K_RETURN:
                    if fonts.get_width(name_box.font, name_box.written_text) <= MAX_WIDTH:
                        name_box.written_text += event.unicode
                name_box.draw(screen, add_to_end="_")
                pygame.display.flip()

        clock.tick(30)

    
    entire_program_state.player_name = name_box.written_text


    return next_scene # return next scene to be portrayed to the scene variable

if __name__ == "__main__":
    entire_program_state = Entire_program_state() 
    # Very first things that only happen once when starting
    music.initialize_playing_music(pygame.mixer.music)
    splash_text = get_splash_text() # splash text is only initialized once per restart of game
    scene = main
    while scene is not None:
        scene = scene() # scene variable always takes the next scene and runs it again until it's None and program exits.

    pygame.quit()
    sys.exit()



